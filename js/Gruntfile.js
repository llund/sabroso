'use strict';

var rewrite = require('connect-modrewrite');

module.exports = function(grunt) {

  grunt.initConfig({
    browserify:     {
      pkg: grunt.file.readJSON('package.json'),
      options:      {
        transform:  [require('grunt-react').browserify, "browserify-shim"]
      },
      app:          {
        src:        'app/main.js',
        dest:       'build/main.js'
      },
      specs: {
  			src: ["spec/vega-widget-spec.js"],
  			dest: "spec/built-specs.js"
  		}
    },
    connect: {
        'static': {
          options: {
            hostname: 'localhost',
            port: 8000,
            keepalive: true,
            middleware: function(connect, options, middlewares) {

                // the rules that shape our mod-rewrite behavior
                var rules = [
                    '!\\.html|\\.js|\\.css|\\.svg|\\.jp(e?)g|\\.png|\\.ico|\\.gif|\\.eot|\\.svg|\\.ttf|\\.woff|\\.woff2$ /index.html'
                ];

                // add rewrite as first item in the chain of middlewares
                middlewares.unshift(rewrite(rules));

                return middlewares;
            }
          }
        },
    },
    jasmine: {
  		tests: {
  			src: [],
  			options: {
                vendor: [
                    "bower_components/jquery/dist/jquery.min.js",
                    "bower_components/bootstrap/dist/js/bootstrap.min.js",
                    "bower_components/d3/d3.min.js"
                ],
  				outfile: "spec/_SpecRunner.html",
                host: 'http://127.0.0.1:8000/',
                keepRunner: true,
  				specs: "spec/built-specs.js"
  			}
  		}
  	},
    jsvalidate: {
        options:{
            globals: {},
            esprimaOptions: {},
            verbose: false
        },
        filesSrc: ['spec/built-specs.js']
    },
    "browserify-shim": {
        "vega": {
            "exports": "global:vg",
            "depends": ["d3"]
        }
    },
    watch: {
        files: ["app/**/*.js"],
        tasks: ['test']
    }
  });

  // Load the plugins
  grunt.loadNpmTasks('grunt-react');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-jsvalidate');

  // Task(s).
  grunt.registerTask('build', ['browserify']);
  grunt.registerTask("test", ["browserify:specs", "jsvalidate", "connect", "jasmine"]);

};
