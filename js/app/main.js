var Constants = require('constants');

//Dev Tools
var createDevTools = require('redux-devtools').createDevTools,
    LogMonitor = require('redux-devtools-log-monitor'),
    DockMonitor = require('redux-devtools-dock-monitor');

//Dependencies
var React = require('react'),
    ReactDOM = require('react-dom'),
    createStore = require('redux').createStore,
    combineReducers = require('redux').combineReducers,
    applyMiddleware = require('redux').applyMiddleware,
    Provider =  require('react-redux').Provider,
    Router = require('react-router').Router,
    Route = require('react-router').Route,
    IndexRoute = require('react-router').IndexRoute,
    browserHistory = require('react-router').browserHistory,
    syncHistoryWithStore = require('react-router-redux').syncHistoryWithStore,
    routerReducer = require('react-router-redux').routerReducer,
    babelPolyfill = require('babel-polyfill'),
    thunkMiddleware = require('redux-thunk').default;

//Components
var App = require('./components/App.jsx'),
    Home = require('./components/Home.jsx'),
    Project = require('./components/Project.jsx'),
    Analysis = require('./components/Analysis.jsx'),
    User = require('./components/User.jsx');

//Reducers
var MenuReducer = require('./reducers/menu.js'),
    AnalysisReducer = require('./reducers/analysis.js'),
    AuthenticationReducer = require('./reducers/auth.js'),
    TransformedDataReducer = require('./reducers/transformed_data.js'),
    UserStateReducer = require('./reducers/user.js'),
    ControlsReducer = require('./reducers/controls.js'),
    FiltersReducer = require('./reducers/filters.js'),
    DataTypesReducer = require('./reducers/data_types.js'),
    ProjectDataReducer = require('./reducers/project_data.js'),
    AnalysisTypesReducer = require('./reducers/analysis_types.js'),
	ArgumentsReducer = require('./reducers/arguments.js'),    
	AccountReducer = require('./reducers/account.js'),
    ConnectedProjectDataReducer = require('./reducers/connected_project_data.js'),
    UsersReducer = require('./reducers/users.js');

var reducer = combineReducers({
    menu: MenuReducer,
    analysis: AnalysisReducer,
    auth: AuthenticationReducer,
    analysisData: TransformedDataReducer,
    userState: UserStateReducer,
    controlOptions: ControlsReducer,
    filters: FiltersReducer,
    args: ArgumentsReducer,
    dataTypes: DataTypesReducer,
    projectData: ProjectDataReducer,
    analysisTypes: AnalysisTypesReducer,
    routing: routerReducer,
    account: AccountReducer,
    connectedProjectData: ConnectedProjectDataReducer,
    users: UsersReducer,
});

//var DevTools = createDevTools(
//    <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-q">
//        <LogMonitor theme="tomorrow" preserveScrollTop={false} />
//    </DockMonitor>
//);
var rootReducer = (state, action) => {
  if (action.type === Constants.LOGOUT_USER) {
    state = undefined
  }
  return reducer(state, action);
}

var store = createStore(
        //reducer,
        rootReducer,
        applyMiddleware(
            thunkMiddleware
        )
//      DevTools.instrument()
    ),
    history = syncHistoryWithStore(browserHistory, store);

//<Route path="project/:projectId/:pageId/:analysisId" component={Page}/>
//<DevTools />

ReactDOM.render(
    <Provider store={store}>
        <div>
            <Router history={history}>
                <Route path="/" component={App}>
                    <IndexRoute component={Home}/>
                    <Route path="/project/:projectId" component={Project}/>
                    <Route path="/analysis/:analysisId(/save/:saveId)" component={Analysis}/>
                    <Route path="/user" component={User}/>
                </Route>
            </Router>
        </div>
    </Provider>,
    document.getElementById('application')
)
