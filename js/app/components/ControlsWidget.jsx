var React = require('react'),
    connect = require('react-redux').connect,
    browserHistory = require('react-router').browserHistory,
    Dropdown = require('./Dropdown.jsx'),
    RangeWidget = require('./RangeWidget.jsx'),
    Item = require('./Item.jsx'),
    ControlRows = require('./ControlRows.jsx'),
    pushUserState = require('../actions/user_actions.js').pushUserState,
    fetchControl = require('../actions/control_actions.js').fetchControl,
    fetchData = require('../actions/transformer_actions.js').fetchData,
    Constants = require('../constants');

var DrawControlsWidget = React.createClass({

    _rangeSubmit: function _rangeSubmit(column, bound, value) {
        var toPush = [];
        if (this.props.userControls.hasOwnProperty(column)) {
            toPush = [this.props.userControls[column][0], this.props.userControls[column][1]];
        }
        if (bound === "min") {
            toPush[0] = value;
        } else if (bound === "max") {
            toPush[1] = value;
        }
        this.props.doPush({
            target: "controls",
            method: Constants.ControlTypes.RANGE,
            column: column,
            values: toPush
        }, this.props.analysisId);
    },

    _dropdownSubmit: function _dropdownSubmit(column, actionItems, method) {
        this.props.doPush({
            target: "controls",
            method: method,
            column: column,
            values: actionItems
        }, this.props.analysisId);
        if (method === Constants.ControlTypes.CONFIGURATION_SELECTION) {
            var values = {};
            actionItems[0].value.values.forEach(function(value, i) {
                var path = actionItems[0].value.paths[i],
                    key = path.join(',');
                values[key] = {
                    value: value,
                    path: path
                };
            });
            this.props.doPush({
                target: "configurationUpdates",
                column: column,
                method: method,
                values: values
            }, this.props.analysisId);
        }
    },

    _doSubmit: function _doSubmit() {
        this.props.pullData(
            this.props.pivotConfiguration, this.props.filters, this.props.args,
            this.props.projectId, this.props.postHeader);
    },

    _makeFilters: function _makeFilters(filters) {
        var controls = [];
        filters.forEach(function(oneControl) {
            switch (oneControl.method) {
                case Constants.ControlTypes.SELECTION:
                    var activeItem;
                    column = oneControl.data_type+','+oneControl.column
                    if (this.props.userControls.hasOwnProperty(column)) {
                        activeItem = this.props.userControls[column];
                    }
                    controls.push(
                        <Dropdown
                            menuItems={ this.props.controlOptions[column] }
                            title={ oneControl.label }
                            updateFunc={ this._dropdownSubmit }
                            activeItem={ activeItem }
                            dropdownKey={ column }
                            key={ column }
                            method={ Constants.ControlTypes.SELECTION }
                            multi={ true }
                            size={ 6 }
                        />
                    );
                    break;
                case Constants.ControlTypes.RANGE:
                    var minVal, maxVal;
                    column = oneControl.data_type+','+oneControl.column
                    if (this.props.userControls.hasOwnProperty(column)) {
                        minVal = this.props.userControls[column][0];
                        maxVal = this.props.userControls[column][1];
                    }
                    controls.push(
                        <RangeWidget
                            title={ oneControl.label }
                            updateFunc={ this._rangeSubmit }
                            minVal={ minVal }
                            maxVal={ maxVal }
                            dropdownKey= { column }
                            key={ column }
                            size={ 6 }
                        />
                    );
                    break;
            }
        }, this);
        return controls;
    },

    _makeArguments: function _makeArguments(argControls) {
        var controls = [];
        argControls.forEach(function(oneControl) {
            var menuItems = [],
                activeItem = null;
            oneControl.options.forEach(function(option, i) {
                menuItems.push({
                    value: option,
                    label: oneControl.option_labels[i]
                });
            });
            column = oneControl.function_name+','+oneControl.argument
            if (this.props.userControls.hasOwnProperty(column)) {
                activeItem = this.props.userControls[column];
            }
            multipleSelect = (oneControl.hasOwnProperty('multiple_select')) ? oneControl.multiple_select : true;
            controls.push(
                <Dropdown
                    menuItems={ menuItems }
                    title={ oneControl.label }
                    updateFunc={ this._dropdownSubmit }
                    activeItem={ activeItem }
                    dropdownKey={ column }
                    key={ column }
                    method={ Constants.ControlTypes.ARGUMENT_SELECTION }
                    multi = { multipleSelect }
                    size = { 6 }
                />
            );
        }, this);
        return controls;
    },

    _makeConfiguration: function _makeConfiguration(confControls) {
        var controls = [];
        confControls.forEach(function(oneControl) {
            var menuItems = [],
                activeItem = null,
                column = oneControl.label;
            oneControl.options.forEach(function(option, i) {
                menuItems.push({
                    value: {
                        values: option,
                        paths: oneControl.paths
                    },
                    label: oneControl.option_labels[i]
                });
            });
            if (this.props.userControls.hasOwnProperty(column)) {
                activeItem = this.props.userControls[column];
            }
            controls.push(
                <Dropdown
                    menuItems={ menuItems }
                    title={ oneControl.label }
                    updateFunc={ this._dropdownSubmit }
                    activeItem={ activeItem }
                    dropdownKey={ column }
                    key={ column }
                    method={ Constants.ControlTypes.CONFIGURATION_SELECTION }
                    multi={ false }
                    notClearable={ true }
                    size= { 6 }
                />
            );
        }, this);
        return controls;
    },

    componentDidMount: function componentDidMount() {
        if (this.props.configuration.hasOwnProperty("filters")) {
            this.props.configuration.filters.forEach(function(oneControl) {
                if (oneControl.method === Constants.ControlTypes.SELECTION) {
                    this.props.pullControl(
                        oneControl.data_type, this.props.projectId, oneControl.column, this.props.authHeader,
                        oneControl.map
                    );
                }
            }, this);
        }
    },

    componentDidUpdate: function componentDidUpdate() {
        if (!this.props.dataIsCurrent) {
            this._doSubmit();
        }
    },

    render: function render() {
        var filterControls = [],
            plotControls = [],
            column = null;

        if (this.props.configuration.hasOwnProperty("filters")) {
            filterControls = filterControls.concat(this._makeFilters(this.props.configuration.filters));
        }

        if (this.props.configuration.hasOwnProperty("arguments")) {
            filterControls = filterControls.concat(this._makeArguments(this.props.configuration.arguments));
        }

        if (this.props.configuration.hasOwnProperty("configuration")) {
            plotControls = plotControls.concat(this._makeConfiguration(this.props.configuration.configuration));
        }

        return (
            <div className="row">
                <div className="col-xs-6">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-xs-6">
                                    <h4><strong>Data controls</strong></h4>
                                </div>
                            </div>
                            <ControlRows controls={ filterControls } rowLength= { 2 } />
                            <div className="row">
                                <div className="col-xs-2">
                                    <button onClick={ this._doSubmit } className="btn btn-default">
                                        <p className="button-text">
                                            <strong>
                                                Submit
                                            </strong>
                                        </p>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-xs-6">
                    <div className="panel panel-default">
                        <div className="panel-body">
                            <div className="row">
                                <div className="col-xs-6">
                                    <h4><strong>Plot controls</strong></h4>
                                </div>
                            </div>
                            <ControlRows controls={ plotControls } rowLength= { 2 } />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

var ControlsWidget = connect(
    function(state, ownProps) {
        return {
            configuration: ownProps.configuration,
            projectId: ownProps.projectId,
            analysisId: ownProps.analysisId,
            pivotConfiguration: ownProps.pivotConfiguration,
            userControls: state.userState.controls,
            controlOptions: state.controlOptions.controls,
            postHeader: state.auth.postHeader,
            authHeader: state.auth.header,
            filters: state.filters,
            args: state.args,
            dataIsCurrent: state.analysisData.isCurrent
        };
    },
    function(dispatch) {
        return {
            doPush: function doPush(userAction, analysisId) {
                var url = '/analysis/'+analysisId+'/';
                dispatch(pushUserState(userAction));
                browserHistory.push(url);
            },
            pullControl: function pullControl(dataType, projectId, column, authHeader, itemMap) {
                dispatch(fetchControl(dataType, projectId, column, authHeader, itemMap));
            },
            pullData: function pullData(pivotConfiguration, filters, args, projectId, postHeader) {
                dispatch(fetchData(pivotConfiguration, filters, args, projectId, postHeader));
            }
        };
    }
)(DrawControlsWidget);

module.exports = ControlsWidget;
