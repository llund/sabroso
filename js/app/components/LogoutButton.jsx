var React = require('react'),
    connect = require('react-redux').connect,
    browserHistory = require('react-router').browserHistory,
    logoutUser = require('../actions/logout_button_actions.js').logoutUser;

var DrawLogoutButton = React.createClass({

  clickLogout: function clickLogout() {
      this.props.logout();
      browserHistory.push('/');
  },

  render: function render() {
    return(
      <button onClick={this.clickLogout} className="btn btn-default">
      <p className="button-text">
          <strong>
              Logout
          </strong>
      </p>
      </button>
    );
  },

});

var LogoutButton = connect(
  function(state) {
      return { };
  },
  function(dispatch) {
      return {
          logout: function logout() {
              dispatch(logoutUser());
          },
      };
  }
)(DrawLogoutButton);

module.exports = LogoutButton;
