var React = require('react'),
    connect = require('react-redux').connect;

var LoadingIndicator = React.createClass({

  propTypes: {
      id: React.PropTypes.string.isRequired,
      isErrorFetching: React.PropTypes.bool.isRequired,
  },

  render: function render() {
    if( this.props.isErrorFetching ) {
      return (
          <div id={this.props.id} className="col-md-12 text-center">
            <div className="loading-indicator text-center">
                <div className="panel-info row alert alert-danger">
                  <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  <h4>Could not load resource</h4>
                </div>
            </div>
          </div>
      );
    }
    return (
        <div id={this.props.id} className="col-md-12 text-center">
          <div className="loading-indicator text-center">
              <div className="panel-info row alert alert-info">
                <h4>Loading...</h4>
                <img src="/img/loading.gif"></img>
              </div>
          </div>
        </div>
    );

  },

});

module.exports = LoadingIndicator;
