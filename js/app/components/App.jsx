var React = require('react'),
    LinkedStateMixin = require('react-addons-linked-state-mixin'),
    connect = require('react-redux').connect,
    fetchToken = require('../actions/auth_actions.js').fetchToken,
    loadCookieToken = require('../actions/auth_actions.js').loadCookieToken,
    logout = require('../actions/auth_actions.js').logout;

var DrawApp = React.createClass({

    mixins: [LinkedStateMixin],

    _submit: function _submit() {
        this.props.pullToken(this.state.user, this.state.password);
    },

    getInitialState: function getInitialState() {
        return {
            user: '',
            password: ''
        };
    },

    componentDidMount: function componentDidMount() {
      this.props.syncCookie();
    },

    render: function render() {
        if (this.props.token) {
            return (
                <div id="page">
                    { this.props.children }
                </div>
            );
        } else {
            var error_message = <div />
            if(true === this.props.isTokenError) {
              error_message = <h4>Invalid username or password</h4>
            }
            return (
                <div id="page">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6 col-md-offset-3">
                                <div id="log-in-form" className="row">
                                    <h3 className="strong">Log in</h3>
                                    { error_message }
                                    <form>
                                        <div className="form-group">
                                            <label className="sr-only" htmlFor="InputUsername">Username</label>
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    <span className="glyphicon glyphicon-user" aria-hidden="true">
                                                    </span>
                                                </div>
                                                <input type="text" className="form-control" id="InputUsername" placeholder="Username"  valueLink={this.linkState('user')} />
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="sr-only" htmlFor="InputPassword">Password</label>
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    <span className="glyphicon glyphicon-lock" aria-hidden="true"></span>
                                                </div>
                                                <input type="password" className="form-control" id="InputPassword" placeholder="Password"  valueLink={this.linkState('password')} />
                                            </div>
                                        </div>
                                        <button type="button" onClick={ this._submit } className="btn btn-default">
                                            <p className="button-text">
                                                <strong>
                                                    Submit
                                                </strong>
                                            </p>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
});

var App = connect(
    function(state) {
        return {
            token: state.auth.token,
            username: state.auth.user,
            isTokenError: state.auth.isTokenError,
        };
    },
    function(dispatch) {
        return {
            pullToken: function pullToken(username, password) {
                dispatch(fetchToken(username, password));
            },
            syncCookie: function syncCookie() {
              dispatch(loadCookieToken());
            },
            logoutUser: function logoutUser() {
              dispatch(logout());
            },
        };
    }
)(DrawApp);

module.exports = App;
