var React = require('react'),
    connect = require('react-redux').connect,
    Immutable = require('immutable'),
    Vega = require('vega'),
    plotRedraw = require('../actions/user_actions.js').plotRedraw;

var DrawVegaChart = React.createClass({

    _view: null,

    _buildConfiguration: function _buildConfiguration() {
        var plotConfiguration = Immutable.fromJS(this.props.configuration);
        Object.keys(this.props.configurationUpdates).forEach(function(key) {
            Object.keys(this.props.configurationUpdates[key]).forEach(function(subKey) {
                var path = this.props.configurationUpdates[key][subKey].path,
                    value = this.props.configurationUpdates[key][subKey].value;
                plotConfiguration = plotConfiguration.setIn(path, value);
            }, this);
        }, this);
        return plotConfiguration.toJS();
    },

    _updateChart: function _updateChart() {
        this._view.data("table")
            .remove(function(d) { return true; })
            .insert(this.props.data);
        this._view.data("meta")
            .remove(function(d) { return true; })
            .insert(this.props.meta);
        this._view.update();
        this.props.redrawComplete();
    },

    _drawChart: function _drawChart() {
        var el = this.getDOMNode(),
            thiz = this,
            renderer = this.props.renderer ? this.props.renderer : "canvas",
            config = thiz._buildConfiguration();
        Vega.parse.spec(config,
            function(chart) {
                thiz._view = chart({
                    'el': el,
                    'renderer': renderer
                });
                thiz._updateChart();
            }
        );
    },

    propTypes: {
        name: React.PropTypes.string.isRequired,
        renderer: React.PropTypes.string,
        configuration: React.PropTypes.object.isRequired
    },

    componentDidMount: function() {
        this._drawChart();
    },

    componentDidUpdate: function() {
        if (this.props.redraw) {
            if (this.props.updateConfiguration) {
                this._drawChart();
            } else if (this._view) {
                this._updateChart();
            }
        }
    },

    render: function() {
        return (
            <div id={this.props.name}></div>
        );
    }
});

var VegaChart = connect(
    function(state, ownProps) {
        return {
            name: ownProps.name,
            data: state.analysisData.data,
            meta: state.analysisData.meta,
            configurationUpdates: state.userState.configurationUpdates,
            configuration: ownProps.configuration,
            updateConfiguration: state.userState.updateConfiguration,
            redraw: state.userState.requireRedraw
        };
    },
    function(dispatch) {
        return {
            redrawComplete: function() {
                dispatch(plotRedraw(false));
            }
        };
    }
)(DrawVegaChart);

module.exports = VegaChart;
