var React = require('react'),
    connect = require('react-redux').connect,
    SelectField = require('react-select'),
    PageTitle = require('./PageTitle.jsx'),
    ActionMenu = require('./ActionMenu.jsx'),
    Dropdown = require('./Dropdown.jsx'),
    fetchAnalyses = require('../actions/analysis_actions.js').fetchAnalyses,
    fetchDataTypes = require('../actions/data_type_actions.js').fetchDataTypes,
    addData = require('../actions/project_data.js').addData,
    listData = require('../actions/project_data.js').listData,
    fetchProjectListOfDataType = require('../actions/project_actions.js').fetchProjectListOfDataType,
    assignDataToProject = require('../actions/project_data.js').assignDataToProject,
    fetchUsers = require('../actions/account_actions.js').fetchUsers,
    shareProject = require('../actions/project_actions.js').shareProject,
    clearMessages = require('../actions/message_actions.js').clearMessages;

var DrawProject = React.createClass({

    _pullData: function _pullData() {
        var reader = new FileReader(),
            _this = this;
        reader.onload = function(e) {
            var contents = e.target.result;
            _this._submitData(
                d3.csv.parse(contents).map(function(row) {
                    var ret = {};
                    Object.keys(row).forEach(function(key) {
                        if (row[key] !== "") {
                            if (!isNaN(+row[key])) {
                                ret[key] = +row[key];
                            } else {
                                ret[key] = row[key];
                            }
                        } else {
                            ret[key] = null;
                        }
                    });
                    return ret;
                })
            );
        };
        reader.readAsText(this.state.dataFile);
    },

    _submitData: function _submit(data) {
        this.props.uploadData(data, this.props.params.projectId, this.state.dataType.value, this.props.postHeader);
    },

    _setFile: function _setFile(event) {
        this.setState({
            dataFile: event.target.files[0]
        });
    },

    _updateDataType: function _updateDataType(value) {
        this.setState({
            dataType: value
        });
    },

    _updateProjectDataType: function _updateProjectDataType(value) {
        this.setState({
            projectDataType: value,
            assignProjectData: null
        });
        this.props.pullProjectDataList(value.value, this.props.authHeader);
    },

    _setAssignProjectData: function _setAssignProjectData(value) {
        this.setState({
            assignProjectData: value
        });
    },

    _useAssignProjectData: function _useAssignProjectData() {
      this.props.assignData(this.props.activeProject.id, this.state.projectDataType.value, this.state.assignProjectData.value, this.props.postHeader);
    },

    _updateShareUser: function _updateShareUser(value) {
        this.setState({
          shareUser: value
        });
    },

    _shareProject: function _shareProject() {
      this.props.doShareProject(this.props.activeProject.id, this.state.shareUser.value, this.props.authHeader, this.props.postHeader);
    },

    getInitialState: function getInitialState() {
        return {
            dataType: '',
            projectDataType: '',
            dataFile: null,
            projectData: null,
            connectedProjectData: {data_list: []},
            shareUser: '',
            userList: [],
            assignProjectData: null,
        };
    },

    componentDidMount() {
        this.props.pullAnalyses(this.props.params.projectId, this.props.authHeader);
        this.props.pullDataTypes(this.props.authHeader);
        this.props.pullConnectedProjectData(this.props.params.projectId, this.props.authHeader);
        this.props.pullUsers(this.props.authHeader);
        this.props.doClearMessages();
    },

    render: function render() {
        var excludeFromShareList = this.props.username;

        var path = [],
            activeProjectId,
            statusStatement = (
                <div></div>
            ),
            assignStatusStatement = (
              <div></div>
            ),
            shareMessage = (
              <div></div>
            ),
            dataTypeOptions = this.props.dataTypes.map(function (dataType){
                return {
                    label: dataType.name,
                    value: dataType.id
                };
            }),
            projectDataTypeOptions = dataTypeOptions,
            projectDataOptions = this.props.assignProjectData.map(function(pData){
              return {
                label: pData.title,
                value: pData.project_id
              };
            }),
            userSelectOptions = this.props.userList.map(function(uData){
                return {
                  label: uData.username,
                  value: uData.id
                };
            }),
            connectedDataList = this.props.connectedProjectData.data_list.map(function(item, i){
              return (
                <div key={item.key}>{i+1}: {item.value}</div>
              );
            });

        // remove selected user from share with list
        userSelectOptions = userSelectOptions.filter(function(item) {
          return item.label !== excludeFromShareList;
        });

        if (this.props.activeProject) {
            path = [
                {
                    name: this.props.activeProject.name,
                    itemType: "Project",
                    id: this.props.activeProject.id
                }
            ];
            activeProjectId = this.props.activeProject.id;
        }
        if (this.props.projectData.isFetching) {
            statusStatement = (
                <div className="alert alert-info" role="alert">Fetching...</div>
            );
        } else if (this.props.projectData.isSuccessful) {
            statusStatement = (
                <div className="alert alert-success" role="alert">Successful upload.</div>
            );
        }
        if(this.props.projectData.isAssigning) {
          assignStatusStatement = (
            <div className="alert alert-info" role="alert">Assigning...</div>
          );
        } else if (this.props.projectData.isAssignSuccess) {
          assignStatusStatement = (
              <div className="alert alert-success" role="alert">Successfully assigned.</div>
          );
        }
        if('error' in this.props.projectData.shareMessage) {
          shareMessage = (
            <div className="alert alert-danger" role="alert">{this.props.projectData.shareMessage.error}</div>
          );
        }
        else if ('success' in this.props.projectData.shareMessage) {
          shareMessage = (
            <div className="alert alert-success" role="alert">{this.props.projectData.shareMessage.success}</div>
          );
        }

        return (
            <div className="container">
                <div id="navigation" className="row size-seven">
                    <PageTitle path={path} username={this.props.username}/>
                </div>
                <div id="action-menu" className="row">
                    <ActionMenu menuItems={this.props.menuItems} itemType={"Analysis"} activeProject={activeProjectId}/>
                </div>
                <div id="page-break" className="row">
                    <br />
                </div>
                <div id="dashboard" className="row">
                    <div className="col-md-12 functional-block">
                        <div className="row">
                            <div className="col-md-3">
                                <h3><strong>Upload data</strong></h3>
                                <div className="row">
                                    <div className="col-md-9 form-padding">
                                        <form>
                                            <div className="form-group">
                                                <SelectField name="data-type-select" placeholder="Choose a data type..." value={this.state.dataType} options={dataTypeOptions} onChange={this._updateDataType}/>
                                            </div>
                                            <div className="form-group">
                                                <input type="file" className="form-control" id="input-item-file" placeholder="File" onChange={this._setFile}/>
                                            </div>
                                            <div className="form-group">
                                                <button onClick={this._pullData} type="button" className="btn btn-default">
                                                    <p className="button-text">
                                                        <strong>
                                                            Upload
                                                        </strong>
                                                    </p>
                                                </button>
                                            </div>
                                            { statusStatement }
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3">
                                <h3><strong>Select data</strong></h3>
                                <div className="row">
                                    <div className="col-md-9 form-padding">
                                        <form>
                                            <div className="form-group">
                                                <SelectField name="project-data-type-select" placeholder="Choose a data type..." value={this.state.projectDataType} options={projectDataTypeOptions} onChange={this._updateProjectDataType}/>
                                            </div>
                                            <div className="form-group">
                                                <SelectField name="project-data-select" placeholder="Select existing data..." value={this.state.assignProjectData} options={projectDataOptions} onChange={this._setAssignProjectData}/>
                                            </div>
                                            <div className="form-group">
                                                <button onClick={this._useAssignProjectData} type="button" className="btn btn-default">
                                                    <p className="button-text">
                                                        <strong>
                                                            Use Selected
                                                        </strong>
                                                    </p>
                                                </button>
                                            </div>
                                            { assignStatusStatement }
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3">
                            <h3><strong>Share project</strong></h3>
                            <div className="row">
                              <div className="col-md-9 form-padding">
                              <form>
                                  <div className="form-group">
                                      <SelectField name="user-share-select" placeholder="Select user to share project with..." value={this.state.shareUser} options={userSelectOptions} onChange={this._updateShareUser}/>
                                  </div>
                                  <div className="form-group">
                                      <button onClick={this._shareProject} type="button" className="btn btn-default">
                                          <p className="button-text">
                                              <strong>
                                                  Share Project
                                              </strong>
                                          </p>
                                      </button>
                                  </div>
                                  { shareMessage }
                              </form>
                              </div>
                          </div>
                        </div>
                        <div className="col-md-3">
                          <h3><strong>Data summary</strong></h3>
                          { connectedDataList }
                        </div>
                    </div>
                </div>
            </div>
          </div>
        );
    }
});

var Project = connect(
    function(state) {
        return {
            menuItems: state.menu.analyses,
            dataTypes: state.dataTypes.dataTypes,
            assignProjectData: state.menu.projectsOfType,
            activeProject: state.menu.project,
            projectData: state.projectData,
            connectedProjectData: state.connectedProjectData,
            authHeader: state.auth.header,
            postHeader: state.auth.postHeader,
            username: state.auth.user,
            userList: state.users.users
        };
    },
    function(dispatch) {
        return {
            pullAnalyses: function pullAnalyses(projectId, authHeader) {
                dispatch(fetchAnalyses(projectId, authHeader));
            },
            pullDataTypes: function pullDataTypes(authHeader) {
                dispatch(fetchDataTypes(authHeader));
            },
            uploadData: function uploadData(data, dataType, projectId, authHeader) {
                dispatch(addData(data, dataType, projectId, authHeader));
            },
            pullProjectDataList: function pullProjectDataList(dataType, authHeader) {
                dispatch(fetchProjectListOfDataType(dataType, authHeader));
            },
            assignData: function assignData(projectId, dataType, datasetId, postHeader) {
                dispatch(assignDataToProject(projectId, dataType, datasetId, postHeader));
            },
            pullConnectedProjectData: function pullConnectedProjectData(projectId, authHeader) {
                dispatch(listData(projectId, authHeader));
            },
            pullUsers: function pullUsers(authHeader) {
              dispatch(fetchUsers(authHeader));
            },
            doShareProject: function doShareProject(projectId, shareUser, authHeader, postHeader) {
              dispatch(shareProject(projectId, shareUser, authHeader, postHeader));
            },
            doClearMessages: function doClearMessages() {
              dispatch(clearMessages());
            },
        };
    }
)(DrawProject);

module.exports = Project;
