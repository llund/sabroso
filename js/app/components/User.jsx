var React = require('react'),
    connect = require('react-redux').connect,
    LinkedStateMixin = require('react-addons-linked-state-mixin'),
    changePassword = require('../actions/account_actions.js').changePassword,
    PageTitle = require('./PageTitle.jsx');

var DrawUser = React.createClass({
    mixins: [LinkedStateMixin],

    getInitialState: function getInitialState() {
        return {
            password: '',
            password2: ''
        };
    },

    componentDidMount: function componentDidMount() {
        // this.props.pullUser(this.props.authHeader);
    },

    _submitChangePassword: function _submitChangePassword() {
        this.props.changePasswordUser(this.props.postHeader, this.state.password, this.state.password2);
    },

    render: function render() {

        var path = [
            {
                name: "User",
                itemType: "User",
                id: this.props.username,
                noLink: true,
            }
        ];

        var feedback_message = <div />
        if(null !== this.props.message) {
          feedback_message = <h3>{this.props.message}</h3>
        }

        return (
            <div className="container">
                <div id="navigation" className="row size-seven">
                    <PageTitle path={path} username={this.props.username} />
                </div>

                <div className="row">
                  <div className="col-md-12 functional-block">
                      <div className="navbar">
                          <h4 className="subtitle navbar-text">
                              <strong>Change password</strong>
                          </h4>
                      </div>
                      <div className="row">
                        <div className="col-md-4">
                        { feedback_message }
                        <form>
                          <p>
                              <label htmlFor="InputPassword">New Password</label>
                              <input type="password" className="form-control" id="InputPassword" placeholder="New Password"  valueLink={this.linkState('password')} />
                          </p>
                          <p>
                              <label htmlFor="ConfirmPassword">Confirm Password</label>
                              <input type="password" className="form-control" id="ConfirmPassword" placeholder="Confirm Password"  valueLink={this.linkState('password2')} />
                          </p>
                          <p>
                            <button type="button" onClick={ this._submitChangePassword } className="btn btn-default">
                                <p className="button-text">
                                    <strong>
                                        Submit
                                    </strong>
                                </p>
                            </button>
                          </p>
                        </form>
                        </div>
                        <div className="col-md-8">&nbsp;</div>
                      </div>
                  </div>
                </div>

            </div>
        );
    }
});

var User = connect(
    function(state) {
        return {
            authHeader: state.auth.header,
            postHeader: state.auth.postHeader,
            username: state.auth.user,
            isPasswordChangeError: state.account.isPasswordChangeError,
            message: state.account.message,
        };
    },
    function(dispatch) {
        return {
            changePasswordUser: function changePasswordUser(authHeader, p1 ,p2) {
                dispatch(changePassword(authHeader, p1, p2));
            }
        };
    }
)(DrawUser);

module.exports = User;
