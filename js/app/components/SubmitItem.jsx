var React = require('react'),
    LinkedStateMixin = require('react-addons-linked-state-mixin'),
    connect = require('react-redux').connect,
    SelectField = require('react-select');

//<div className="form-group">
//<label className="sr-only" htmlFor="InputDescription">Description</label>
//<textarea className="form-control" id="Description" placeholder="Description" rows="3" />
//</div>

var SubmitItem =  React.createClass({

    mixins: [LinkedStateMixin],

    propTypes: {
        itemType: React.PropTypes.string.isRequired,
        submitFunction: React.PropTypes.func.isRequired,
        extraOptions: React.PropTypes.object,
        show: React.PropTypes.bool
    },

    _submit: function _submit() {
        var baseSubmit = {
            name: this.state.name,
            description: this.state.description
        };
        Object.keys(this.state.other).forEach(function(key) {
            baseSubmit[this.state.other[key].type] = this.state.other[key].value;
        }, this);
        this.props.submitFunction(baseSubmit);
    },

    _updateState: function _updateState(value) {
        var other = this.state.other;
        other[value.title] = value;
        this.setState({
            "other": other
        });
    },

    getInitialState: function getInitialState() {
        return {
            name: '',
            description: '',
            other: {}
        };
    },

    render: function render() {
        var additionalControls = Object.keys(this.props.extraOptions).map(function(oneOption) {
            return (
                <div key={ oneOption } className="form-group">
                    <SelectField name={oneOption.toLowerCase().replace(' ', '-')+"-select"} placeholder={"Choose a(n) "+oneOption.toLowerCase()+" ..."} value={this.state.other[oneOption]} options={this.props.extraOptions[oneOption]} onChange={this._updateState}/>
                </div>
            );
        }, this);
        if (this.props.show) {
            return (
                <div className="col-md-4">
                    <div className="item">
                        <h4><strong>{"Add "+this.props.itemType.toLowerCase()}</strong></h4>
                        <form>
                            <div className="form-group">
                                <label className="sr-only" htmlFor="InputName">Name</label>
                                <input type="text" className="form-control" id="InputName" placeholder="Name" valueLink={this.linkState('name')}/>
                            </div>
                            <div className="form-group">
                                <label className="sr-only" htmlFor="InputDescription">Description</label>
                                <textarea className="form-control" id="InputDescription" placeholder="Description" valueLink={this.linkState('description')} rows={3}/>
                            </div>
                            { additionalControls }
                            <button type="button" onClick={ this._submit } className="btn btn-default">
                                <p className="button-text">
                                    <strong>
                                        Submit
                                    </strong>
                                </p>
                            </button>
                        </form>
                    </div>
                </div>
            );
        } else {
            return (<div />);
        }
    }
});

module.exports = SubmitItem;
