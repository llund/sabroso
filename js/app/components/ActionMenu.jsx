var React = require('react'),
    connect = require('react-redux').connect,
    LoadingIndicator = require('./LoadingIndicator.jsx'),
    Item = require('./Item.jsx'),
    SubmitItem = require('./SubmitItem.jsx'),
    addProject = require('../actions/project_actions.js').addProject,
    addAnalysis = require('../actions/analysis_actions.js').addAnalysis,
    addAnalysisSave = require('../actions/save_actions').addSave,
    fetchAllAnalysisConfiguration = require('../actions/analysis_configuration_actions.js').fetchAllAnalysisConfiguration;

function makeTitle(itemType) {
    var title = itemType + 's';
    switch (itemType) {
        case 'Analysis':
            title = 'Analyses';
            break;
    }
    return title;
}

var DrawActionMenu =  React.createClass({

    propTypes: {
        menuItems: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        itemType: React.PropTypes.string.isRequired,
        activeProject: React.PropTypes.string,
        activeAnalysis: React.PropTypes.string,
        activeAnalysisSave: React.PropTypes.string,
        superItemType: React.PropTypes.string,
        superId: React.PropTypes.string
    },

    _toggleSubmit: function _toggleSubmit() {
        this.setState({
            submitButton: this.state.submitButton ? false : true,
            submitClass: this.state.submitButton ? "" : "active"
        });
    },

    _doSubmit: function _doSubmit(item) {
        var objId;
        switch (this.props.itemType) {
            case "Analysis":
                objId = this.props.activeProject;
                break;
            case "Save":
                objId = this.props.activeAnalysis;
                item.configuration = {
                    "userState": this.props.userState,
                    "filters": this.props.filters,
                    "args": this.props.args
                };
                break;
        }
        this.props.doSubmit(item, this.props.itemType, this.props.postHeader, this.props.authHeader, objId);
        this._toggleSubmit();
    },

    _getExtraOptions: function _getExtraOptions() {
        switch (this.props.itemType) {
            case "Analysis":
                var analysisTypes = this.props.analysisTypes.filter(function(analysisType) {
                    return analysisType.active_projects.indexOf(this.props.activeProject) !== -1;
                }, this).map(function(analysisType) {
                    return {
                        label: analysisType.name,
                        value: analysisType.id,
                        type: "analysis_type",
                        title: "Analysis type"
                    };
                });
                return {
                    "Analysis type": analysisTypes
                };
            default:
                return {};
        }
    },

    componentDidMount: function componentDidMount() {
        if (this.props.itemType == "Analysis" || this.props.itemType == "Save") {
            this.props.pullAnalysisTypes(this.props.authHeader);
        }
    },

    getInitialState: function getInitialState() {
        return {
            submitButton: false,
            submitClass: ""
        }
    },

    render: function render() {
        var itemTitle = makeTitle(this.props.itemType),
            items = <LoadingIndicator isErrorFetching={ this.props.isErrorFetching } />;

        if(false === this.props.menuIsFetching && false === this.props.isErrorFetching) {
            items = this.props.menuItems.map(function(item) {
                return <Item key={item.id}
                        id={item.id}
                        name={item.name}
                        description={item.description}
                        itemType={this.props.itemType}
                        created_on={item.created_on}
                        superItemType={this.props.superItemType}
                        superId={this.props.superId}/>
            }, this);
        }

        return (
            <div className="col-md-12 functional-block">
                <div className="navbar">
                    <h4 className="subtitle navbar-text">
                        <strong>{itemTitle}</strong>
                    </h4>
                    <button onClick={this._toggleSubmit} className={"btn btn-default " + this.state.submitClass} role="button">
                        <span className="glyphicon glyphicon-plus full-size title-text" aria-hidden="true">
                        </span>
                    </button>
                </div>
                <div className="row">
                    <SubmitItem itemType={ this.props.itemType } submitFunction={ this._doSubmit } show={ this.state.submitButton } extraOptions={ this._getExtraOptions() }/>
                    {items}
                </div>
            </div>
        );
    }
});

var ActionMenu = connect(
    function(state, ownProps) {
        return {
            isErrorFetching: state.menu.isErrorFetching,
            menuIsFetching: state.menu.isFetching,
            menuItems: ownProps.menuItems,
            itemType: ownProps.itemType,
            activeProject: ownProps.activeProject,
            activeAnalysis: ownProps.activeAnalysis,
            postHeader: state.auth.postHeader,
            authHeader: state.auth.header,
            analysisTypes: state.analysisTypes.analysisTypes,
            userState: state.userState,
            filters: state.filters,
            args: state.args
        };
    },
    function(dispatch) {
        return {
            doSubmit: function doSubmit(item, itemType, postHeader, authHeader, objId) {
                switch (itemType) {
                    case "Project":
                        dispatch(addProject(item, postHeader, authHeader));
                        break;
                    case "Analysis":
                        item.project = objId;
                        if (!item.hasOwnProperty("configuration")) {
                            item.configuration = {};
                        }
                        dispatch(addAnalysis(item, postHeader, authHeader));
                        break;
                    case "Save":
                        item.analysis = objId;
                        if (!item.hasOwnProperty("configuration")) {
                            item.configuration = {};
                        }
                        dispatch(addAnalysisSave(item, postHeader, authHeader));
                        break;
                }
            },
            pullAnalysisTypes: function pullAnalysisTypes(authHeader) {
                dispatch(fetchAllAnalysisConfiguration(authHeader));
            }
        };
    }
)(DrawActionMenu);

module.exports = ActionMenu;
