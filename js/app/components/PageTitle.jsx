var React = require('react'),
    Link = require('react-router').Link,
    LogoutButton = require('./LogoutButton.jsx');

var TitleComponent = function (props) {
    var url='/';
    if (props.link.superItemType && props.link.superId) {
        url = url + props.link.superItemType.toLowerCase()+'/'+props.link.superId + '/';
    }
    url = url + props.link.itemType.toLowerCase()+'/'+props.link.id+'/';

    var linkText = <p className="mid-size title-text"><strong>{props.link.name}</strong></p>

    // if(props.link.noLink) {
    //   return (
    //     <li>
    //     {linkText}
    //     </li>
    //   )
    // }
    // else {
      return (
        <li>
        <Link to={url}>{linkText}</Link>
        </li>
      );
    // }
};

TitleComponent.propTypes = {
    link: React.PropTypes.object.isRequired
};

var TitleSeperator = function () {
    return (
        <li>
            <span className="glyphicon glyphicon-chevron-right mid-size title-text simulate-a" aria-hidden="true">
            </span>
        </li>
    );
};


var PageTitle = function PageTitle(props) {
    var items = [],
        seperatorKey,
        componentKey;

    props.path.forEach(function(link) {
        seperatorKey = "Seperator."+link.name;
        componentKey = "Component."+link.name;
        items.push(<TitleSeperator key={seperatorKey} />);
        items.push(<TitleComponent key={componentKey} link={link} />);
    });

    return (
        <div className="col-md-12">
          <nav className="navbar">
            <ul className="nav navbar-nav">
                <li>
                    <Link to={'/'}>
                        <span className="glyphicon glyphicon-home mid-size title-text" aria-hidden="true">
                        </span>
                    </Link>
                </li>
                {items}
            </ul>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <Link to={'/user'}>
                  <span className="glyphicon glyphicon-user mid-size">
                  &nbsp;
                  {props.username}
                  </span>
                </Link>
              </li>
              <li>
                <LogoutButton />
              </li>
            </ul>
          </nav>
        </div>
    );
};


PageTitle.propTypes = {
    path: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    username: React.PropTypes.string.isRequired,
    noLink: React.PropTypes.bool,
};

module.exports = PageTitle;
