var React = require('react'),
    connect = require('react-redux').connect,
    PageTitle = require('./PageTitle.jsx'),
    ActionMenu = require('./ActionMenu.jsx'),
    fetchProjects = require('../actions/project_actions.js').fetchProjects;

var DrawHome = React.createClass({

    componentDidMount: function componentDidMount() {
        this.props.pullProjects(this.props.authHeader);
    },

    render: function render() {

        return (
            <div className="container">
                <div id="navigation" className="row size-seven">
                    <PageTitle path={[]} username={this.props.username} />
                </div>
                <div id="action-menu" className="row">
                    <ActionMenu menuItems={this.props.menuItems} itemType={"Project"}/>
                </div>
            </div>
        );
    }
});

var Home = connect(
    function(state) {
        return {
            menuItems: state.menu.projects,
            authHeader: state.auth.header,
            username: state.auth.user,
        };
    },
    function(dispatch) {
        return {
            pullProjects: function pullProjects(authHeader) {
                dispatch(fetchProjects(authHeader));
            }
        };
    }
)(DrawHome);

module.exports = Home;
