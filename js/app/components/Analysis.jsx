var React = require('react'),
    connect = require('react-redux').connect,
    SelectField = require('react-select'),
    PageTitle = require('./PageTitle.jsx'),
    ActionMenu = require('./ActionMenu.jsx'),
    Constants = require('../constants'),
    VegaWidget = require('./VegaWidget.jsx'),
    LoadingIndicator = require('./LoadingIndicator.jsx'),
    fetchSaves = require('../actions/save_actions.js').fetchSaves,
    fetchAnalysisSave = require('../actions/save_actions.js').fetchSave,
    fetchAnalysisConfiguration = require('../actions/analysis_configuration_actions.js').fetchAnalysisConfiguration,
    fetchUsers = require('../actions/account_actions.js').fetchUsers,
    shareAnalysis = require('../actions/analysis_actions.js').shareAnalysis,
    shareAnalysisSave = require('../actions/save_actions.js').shareAnalysisSave,
    clearCurrentAnalysis = require('../actions/analysis_actions.js').clearCurrentAnalysis,
    clearMessages = require('../actions/message_actions.js').clearMessages;

var DrawAnalysis = React.createClass({

    _buildAnalysisInsert: function _buildAnalysisInsert() {
        switch (this.props.pluginType) {
            case Constants.AnalysisPlugins.VEGA:
                if (this.props.analysisType && this.props.activeProject) {
                    return (
                        <div className="col-md-12">
                            <VegaWidget analysisType= { this.props.analysisType }
                                controlConfiguration= { this.props.plotConfiguration.controls }
                                plotConfiguration= { this.props.plotConfiguration.plot }
                                pivotConfiguration= { this.props.transformationConfiguration }
                                project = { this.props.activeProject.id }
                                analysis = {this.props.activeAnalysis.id }
                            />
                        </div>
                    );
                } else {
                    return (
                        <div className="col-md-12"></div>
                    );
                }
        }
    },

    _fetchData: function _fetchData() {
        if (this.props.params.saveId) {
            this.props.fetchSave(this.props.params.saveId, this.props.authHeader);
        }
        this.props.fetchAllAnalysis(this.props.params.analysisId, this.props.authHeader);
    },

    componentDidMount: function componentDidMount() {
        this.props.clearAnalysis();
        this._fetchData(this.props);
        this.props.pullUsers(this.props.authHeader);
        this.props.doClearMessages();
    },

    componentDidUpdate: function componentDidUpdate(prevProps) {
        if (prevProps.params.saveId !== this.props.params.saveId) {
            this._fetchData();
        }
    },

    _updateShareUser: function _updateShareUser(value) {
        this.setState({
          shareUser: value
        });
    },

    _shareAnalysis: function _shareAnalysis() {
      if(this.state.shareUser.value === null) {
        return false;
      }
      this.props.doShareAnalysis(this.props.activeAnalysis.id, this.state.shareUser.value, this.props.authHeader, this.props.postHeader);
    },

    _shareAnalysisSave: function _shareAnalysisSave() {
      if(this.state.shareUser.value === null) {
        return false;
      }
      this.props.doShareAnalysisSave(this.props.activeAnalysisSave.id, this.state.shareUser.value, this.props.authHeader, this.props.postHeader);
    },

    getInitialState: function getInitialState() {
        return {
            shareUser: '',
            userList: []
        };
    },

    render: function render() {
        var path = [],
            analysisInsert = this._buildAnalysisInsert(),
            actionMenuDiv = (<div />),
            activeSave,
            superItemType,
            loadingIndicatorDiv = (<div />),
            superId,
            analysisShareMessage = (<div />);

        if (this.props.activeProject && this.props.activeAnalysis) {
            superId = this.props.activeAnalysis.id;
            superItemType = "Analysis";
            if (this.props.activeAnalysisSave) {
                activeSave = this.props.activeAnalysisSave.id;
            }
            actionMenuDiv = (
                <ActionMenu
                    menuItems={this.props.menuItems} itemType={"Save"}
                    activeProject={this.props.activeProject.id}
                    activeAnalysis={this.props.activeAnalysis.id}
                    activeSave={activeSave} superItemType={superItemType}
                    superId={superId}/>
            );
            path = [
                {
                    name: this.props.activeProject.name,
                    itemType: "Project",
                    id: this.props.activeProject.id
                },
                {
                    name: this.props.activeAnalysis.name,
                    itemType: "Analysis",
                    id: this.props.activeAnalysis.id
                }
            ];
            if (this.props.activeAnalysisSave) {
                path.push({
                    name: this.props.activeAnalysisSave.name,
                    itemType: "Save",
                    id: this.props.activeAnalysisSave.id,
                    superItemType: "Analysis",
                    superId: this.props.activeAnalysis.id
                });
            }
        }

        var excludeFromShareList = this.props.username;
        var userSelectOptions = this.props.userList.map(function(uData){
            return {
              label: uData.username,
              value: uData.id
            };
        });
        // remove selected user from share with list
        userSelectOptions = userSelectOptions.filter(function(item) {
          return item.label !== excludeFromShareList;
        });

        var shareAnalysisOrSave = (
            <div className="form-group">
                <button onClick={this._shareAnalysis} type="button" className="btn btn-default">
                    <p className="button-text">
                        <strong>
                            Share Analysis
                        </strong>
                    </p>
                </button>
            </div>
        );
        if( this.props.activeAnalysisSave ) {
            shareAnalysisOrSave = (
              <div className="form-group">
                  <button onClick={this._shareAnalysisSave} type="button" className="btn btn-default">
                      <p className="button-text">
                          <strong>
                              Share Analysis Save
                          </strong>
                      </p>
                  </button>
              </div>
            );
        }

        if( this.props.isDoneSharing ) {
          if('error' in this.props.shareMessage) {
            analysisShareMessage = (
              <div className="alert alert-danger" role="alert">{this.props.shareMessage.error}</div>
            );
          }
          else if ('success' in this.props.shareMessage) {
            analysisShareMessage = (
              <div className="alert alert-success" role="alert">{this.props.shareMessage.success}</div>
            );
          }
        } else if(this.props.isSharing) {
          analysisShareMessage = (
            <div className="alert alert-info" role="alert">Sharing...</div>
          );
        }

        if (this.props.isDataLoading || this.props.isErrorFetching) {
            loadingIndicatorDiv = (
                <LoadingIndicator isErrorFetching={ this.props.isErrorFetching } id="vega-widget" />
            );
        }

        return (
            <div className="container">
                <div id="navigation" className="row size-seven">
                    <PageTitle path={path} username={this.props.username} />
                </div>
                <div id="action-menu" className="row">
                    {actionMenuDiv}
                </div>
                <div id="page-break" className="row">
                    <br />
                </div>
                <div id="dashboard" className="row">
                    <div className="col-md-12 functional-block">
                        <div className="row">
                            {loadingIndicatorDiv}
                        </div>
                        <div className="row">
                            { analysisInsert }
                        </div>
                        <div className="row">
                          <div className="col-md-4 form-padding">
                          <form>
                              <div className="form-group">
                                  <SelectField name="user-share-select" placeholder="Select user to share project with..." value={this.state.shareUser} options={userSelectOptions} onChange={this._updateShareUser}/>
                              </div>
                              { shareAnalysisOrSave }
                              { analysisShareMessage }
                          </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

var Analysis = connect(
    function(state) {
        return {
            menuItems: state.menu.saves,
            activeProject: state.menu.project,
            activeAnalysis: state.menu.analysis,
            activeAnalysisSave: state.menu.analysisSave,
            pluginType: state.analysis.pluginType,
            analysisType: state.analysis.analysisType,
            name: state.analysis.name,
            isSharing: state.analysis.isSharing,
            isDoneSharing: state.analysis.isDoneSharing,
            shareMessage: state.analysis.shareMessage,
            plotConfiguration: state.analysis.plotConfiguration,
            transformationConfiguration: state.analysis.transformationConfiguration,
            authHeader: state.auth.header,
            postHeader: state.auth.postHeader,
            username: state.auth.user,
            userList: state.users.users,
            isErrorFetching: state.analysisData.isErrorFetching,
            isDataLoading: state.analysisData.isFetching
        };
    },
    function(dispatch) {
        return {
            fetchAllAnalysis: function fetchAllAnalysis(analysisId, authHeader) {
                dispatch(fetchSaves(analysisId, authHeader));
                dispatch(fetchAnalysisConfiguration(analysisId, authHeader));
            },
            fetchSave: function fetchSave(saveId, authHeader) {
                dispatch(fetchAnalysisSave(saveId, authHeader));
            },
            pullUsers: function pullUsers(authHeader) {
              dispatch(fetchUsers(authHeader));
            },
            doShareAnalysis: function doShareAnalysis(analysisId, shareUser, authHeader, postHeader) {
              dispatch(shareAnalysis(analysisId, shareUser, authHeader, postHeader));
            },
            doShareAnalysisSave: function doShareAnalysisSave(analysisSaveId, shareUser, authHeader, postHeader) {
              dispatch(shareAnalysisSave(analysisSaveId, shareUser, authHeader, postHeader));
            },
            doClearMessages: function doClearMessages() {
              dispatch(clearMessages());
            },
            clearAnalysis: function clearAnalysis() {
                dispatch(clearCurrentAnalysis());
            }
        };
    }
)(DrawAnalysis);

module.exports = Analysis;
