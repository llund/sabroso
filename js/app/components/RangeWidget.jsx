var React = require('react');

var RangeWidget = function drawRangeWidget(props) {
    var width = props.width ? props.width : 200,
        min_id = props.title+"-input-range-min",
        max_id = props.title+"-input-range-max",
        submitFunctionMin = function submitFunctionMin(event) {
            props.updateFunc(props.dropdownKey, "min", event.target.valueAsNumber);
        },
        submitFunctionMax = function submitFunctionMax(event) {
            props.updateFunc(props.dropdownKey, "max", event.target.valueAsNumber);
        };

    return (
        <div className="col-xs-6">
            <div className="row">
                <div className="col-xs-12">
                    <label htmlFor={min_id}>{props.title}</label>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-5">
                    <input onChange={submitFunctionMin} className="form-control" type="number" id={min_id} placeholder="Min" value={props.minVal} />
                </div>
                <div className="col-xs-2">
                    <h4 className="text-center">-</h4>
                </div>
                <div className="col-xs-5">
                    <input onChange={submitFunctionMax} className="form-control" type="number" id={max_id} placeholder="Max" value={props.maxVal}/>
                </div>
            </div>
        </div>
    );
};

RangeWidget.propTypes = {
    title: React.PropTypes.string.isRequired,
    updateFunc: React.PropTypes.func.isRequired,
    minVal: React.PropTypes.number,
    maxVal: React.PropTypes.number,
    dropdownKey: React.PropTypes.string.inRequired
};

module.exports = RangeWidget;
