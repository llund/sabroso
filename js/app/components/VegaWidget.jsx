var React = require('react'),
    connect = require('react-redux').connect,
    VegaChart = require('./VegaChart.jsx'),
    ControlsWidget = require('./ControlsWidget.jsx');

var DrawVegaWidget = React.createClass({

    render: function() {
        var vegaChart = (<div></div>);
        if (this.props.data.length) {
            vegaChart = (<VegaChart
                name={ this.props.analysisType }
                configuration={ this.props.plotConfiguration }
            />);
        }
        return (
            <div id="vega-widget">
                <br />
                <ControlsWidget
                    configuration={this.props.controlConfiguration}
                    pivotConfiguration={this.props.pivotConfiguration}
                    projectId={this.props.projectId}
                    analysisId={this.props.analysisId}
                />
                <br />
                { vegaChart }
            </div>
        );
    }
});

var VegaWidget = connect(
    function(state, ownProps) {
        return {
            analysisType: ownProps.analysisType,
            analysisId: ownProps.analysis,
            projectId: ownProps.project,
            controlConfiguration: ownProps.controlConfiguration,
            pivotConfiguration: ownProps.pivotConfiguration,
            plotConfiguration: ownProps.plotConfiguration,
            data: state.analysisData.data
        };
    },
    function(dispatch) {
        return {};
    }
)(DrawVegaWidget);

module.exports = VegaWidget;
