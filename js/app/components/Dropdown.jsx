var React = require('react'),
    SelectField = require('react-select');

var Dropdown = React.createClass({

    _submit: function _submit(eventObjects) {
        if (!this.props.multi) {
            eventObjects = [eventObjects];
        }
        this.props.updateFunc(this.props.dropdownKey, eventObjects, this.props.method);
    },

    propTypes: {
        menuItems: React.PropTypes.arrayOf(React.PropTypes.object),
        title: React.PropTypes.string.isRequired,
        updateFunc: React.PropTypes.func.isRequired,
        activeItem: React.PropTypes.arrayOf(React.PropTypes.object),
        defaultText: React.PropTypes.string,
        dropdownKey: React.PropTypes.string.isRequired,
        method: React.PropTypes.string.isRequired,
        multi: React.PropTypes.bool.isRequired,
        size: React.PropTypes.number.isRequired,
        notClearable: React.PropTypes.bool
    },

    componentDidMount: function componentDidMount() {
        if (this.props.notClearable) {
            this._submit(this.props.menuItems[0]);
        }
    },

    render: function render() {
        var placeholder = this.props.defaultText ? this.props.defaultText : "Choose option(s)",
            activeItem = this.props.notClearable && this.props.activeItem !== null ? this.props.activeItem[0] : this.props.activeItem,
            clearable = !this.props.notClearable;
        return (
            <div className={ "col-xs-"+this.props.size }>
                <div className="form-group">
                    <label htmlFor={this.props.title.replace(" ", "-")+"-select"}>{this.props.title}</label>
                    <SelectField
                        name={this.props.title.replace(" ", "-")+"-select"}
                        placeholder={placeholder}
                        value={activeItem}
                        options={this.props.menuItems}
                        onChange={this._submit}
                        multi={this.props.multi}
                        clearable={clearable}
                    />
                </div>
            </div>
        );
    }
});

module.exports = Dropdown;
