var React = require('react');

var ControlRow = React.createClass({

    propTypes: {
        items: React.PropTypes.arrayOf(React.PropTypes.object).isRequired
    },

    render: function render() {
        return(
            <div className="row">
                { this.props.items }
            </div>
        );
    },

});

var ControlRows = React.createClass({

    propTypes: {
        controls: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
        rowLength: React.PropTypes.number.isRequired
    },

    render: function render() {
        var groups = [];

        for (var i = 0; i < this.props.controls.length; i += this.props.rowLength) {
            groups.push(<ControlRow key={i} items={ this.props.controls.slice(i, i+this.props.rowLength) } />);
        }

        return(
            <div className="col-xs-12">
                { groups }
            </div>
        );
    },

});

module.exports = ControlRows;
