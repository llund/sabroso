var React = require('react'),
    connect = require('react-redux').connect,
    browserHistory = require('react-router').browserHistory,
    removeProject = require('../actions/project_actions.js').removeProject,
    removeAnalysis = require('../actions/analysis_actions.js').removeAnalysis,
    removeAnalysisSave = require('../actions/save_actions.js').removeAnalysisSave;

var DrawItem =  React.createClass({

    propTypes: {
        id: React.PropTypes.string.isRequired,
        name: React.PropTypes.string.isRequired,
        itemType: React.PropTypes.string.isRequired,
        description: React.PropTypes.string.isRequired,
        superItemType: React.PropTypes.string,
        superId: React.PropTypes.string,
        created_on: React.PropTypes.string
    },

    doClick: function doClick(){
      var url ='/';

      if (this.props.superItemType && this.props.superId) {
          url = url + this.props.superItemType.toLowerCase()+'/'+this.props.superId + '/';
      }
      url = url + this.props.itemType.toLowerCase()+'/'+this.props.id+'/';

      browserHistory.push(url);
    },

    doDeleteClick:  function doDeleteClick() {
      if( confirm("Are you sure you wish to delete this " + this.props.itemType.toLowerCase() + "?")) {
        var props = this.props;
        this.props.doSubmit(this.props.id, props);
      }
    },

    render: function render() {
      var createdOn = this.props.created_on.split('.')[0];

      return (
          <div className="col-md-3">
              <div className="item">
                <div className="row">
                  <div className="col-md-9">
                    <h4><strong>{this.props.name}</strong></h4>
                  </div>
                  <div className="col-md-3 text-right">
                    <button onClick={this.doDeleteClick} className="btn btn-default" role="button">
                      <span className="glyphicon glyphicon-remove-sign full-size" aria-hidden="true">
                      </span>
                    </button>
                  </div>
                </div>

                <div onClick={this.doClick} className="clickable">
                  <p>{this.props.description}</p>
                  <p>Created on: {createdOn.split('T')[0]}</p>
                </div>
              </div>
          </div>
      );
    }
});

var Item = connect(
    function(state, ownProps) {
        return {
          id: ownProps.id,
          name: ownProps.name,
          itemType: ownProps.itemType,
          description: ownProps.description,
          superItemType: ownProps.superItemType,
          superId: ownProps.superId,
          created_on: ownProps.created_on,
          activeProject: state.menu.project,
          activeAnalysis: state.menu.analysis,
          authHeader: state.auth.header,
          postHeader: state.auth.postHeader,
      };
    },
    function(dispatch) {
        return {
            doSubmit: function doSubmit(id, props) {
                switch (props.itemType) {
                    case "Project":
                        dispatch(removeProject(id, props.authHeader, props.postHeader));
                        break;
                    case "Analysis":
                        dispatch(removeAnalysis(id, props.activeProject, props.authHeader, props.postHeader));
                        break;
                    case "Save":
                        dispatch(removeAnalysisSave(id, props.activeAnalysis, props.postHeader, props.authHeader));
                        break;
                }
            }
        };
    }
)(DrawItem);

module.exports = Item;
