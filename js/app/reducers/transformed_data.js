var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    meta: [],
    data: [],
    isCurrent: false,
    isFetching: false,
    isErrorFetching: false
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_TRANSFORMED_DATA:
            return state.merge({
                isFetching: true,
                isCurrent: true
            }).toJS();
        case Constants.RECEIVE_TRANSFORMED_DATA:
            var ret = state;
            if (action.data && action.data.length) {
                ret = ret.merge({
                    isFetching: false,
                    isCurrent: true,
                    data: action.data,
                    meta: action.meta
                });
            }
            return ret.toJS();
        case Constants.ERROR_RECEIVE_TRANSFORMED_DATA:
            return state.merge({
                isErrorFetching: true,
                isFetching: false,
                isCurrent: true,
                data: [],
                meta: []
            }).toJS();
        case Constants.RECEIVE_SAVE:
            return state.merge({
                isCurrent: false
            }).toJS();
        case Constants.CLEAR_ANALYSIS:
            var ret = state.merge({
                isCurrent: false,
                data: [],
                meta: []
            });
            return ret.toJS();
        default:
          return state.toJS();
    }
}

module.exports = update;
