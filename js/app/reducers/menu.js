var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    isFetching: false,
    isErrorFetching: false,
    projects: [],
    analyses: [],
    saves: [],
    projectsOfType: [],
    project: null,
    analysis: null,
    analysisSave: null
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_PROJECTS:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_PROJECTS:
            return state.merge({
                isFetching: false,
                projects: action.projects,
            }).toJS();
        case Constants.REQUEST_PROJECTS_OF_TYPE:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_PROJECTS_OF_TYPE:
            return state.merge({
                isFetching: false,
                projectsOfType: action.projects,
            }).toJS();
        case Constants.REQUEST_ANALYSES:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_ANALYSES:
            return state.merge({
                isFetching: false,
                analyses: action.analyses,
                project: action.project
            }).toJS();
        case Constants.REQUEST_SAVES:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_SAVES:
            return state.merge({
                isFetching: false,
                saves: action.saves,
                analysis: action.analysis,
                project: action.project
            }).toJS();
        case Constants.REQUEST_SAVE:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_SAVE:
            return state.merge({
                isFetching: false,
                analysisSave: action.analysisSave
            }).toJS();
        case Constants.ERROR_RECEIVE_MENU:
            return state.merge({
                isErrorFetching: true,
                isFetching: false,
            }).toJS();
        default:
          return state.toJS()
    }
}

module.exports = update;
