var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    query: {}
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.RECEIVE_SAVE:
            if (action.analysisSave) {
                return action.analysisSave.configuration.filters;
            } else {
                return initialState;
            }
            break;
        case Constants.CLEAR_ANALYSIS:
            return state.merge({
                query: {}
            }).toJS();
        case Constants.PUSH_USER_STATE:
            if (state.hasIn(["query", action.column])) {
                state = state.deleteIn(["query", action.column]);
            }
            switch (action.method) {
                case Constants.ControlTypes.RANGE:
                    var newFilters = {};
                    newFilters[action.column] = {};
                    if (action.values[0] !== undefined) {
                        newFilters[action.column]["$gte"] = action.values[0];
                    }
                    if (action.values[1] !== undefined) {
                        newFilters[action.column]["$lte"] = action.values[1];
                    }
                    return state.mergeDeep({
                        "query": newFilters
                    }).toJS();
                case Constants.ControlTypes.SELECTION:
                    var newFilters = {};
                    if (action.values && action.values.length) {
                        newFilters[action.column] = {'$in': []};
                        action.values.forEach(function(value) {
                            newFilters[action.column]['$in'].push(value.value);
                        });
                        return state.mergeDeep({
                            "query": newFilters
                        }).toJS();
                    } else {
                        return state.toJS();
                    }
                default:
                    state.toJS();
            }
        default:
          return state.toJS();
    }
}

module.exports = update;
