var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    isFetching: false,
    data_list: [],
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }

    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.GETTING_PROJECT_DATA_LIST:
            return state.merge({
                isFetching: true,
                data_list: [],
            }).toJS();
        case Constants.GETTING_PROJECT_DATA_LIST_SUCCESSFUL:
            return state.merge({
                isFetching: false,
                data_list: action.data_list,
            }).toJS();
        default:
            return state.toJS();
    }
}

module.exports = update;
