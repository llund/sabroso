var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    analysisType: null,
    name: "analysis-visualization",
    plotConfiguration: {},
    transformationConfiguration: {},
    pluginType: null,
    isFetching: false,
    shareMessage: {},
    isSharing: false,
    isDoneSharing: false
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_ANALYSIS_CONFIGURATION:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_ANALYSIS_CONFIGURATION:
            var ret = state.merge({
                isFetching: false,
                analysisType: action.analysisType,
                pluginType: action.pluginType,
                plotConfiguration: action.plotConfiguration,
                transformationConfiguration: action.transformationConfiguration
            });
            return ret.toJS();
        case Constants.CLEAR_ANALYSIS:
            var ret = state.merge({
                transformationConfiguration: {},
                plotConfiguration: {},
                pluginType: null,
                analysisType: null
            });
            return ret.toJS();
        case Constants.START_SHARE_ANALYSIS:
          return state.merge({
            isSharing: true,
            isDoneSharing: false,
            shareMessage: action.message
          }).toJS();
        case Constants.COMPLETE_ANALYSIS_SHARE:
          return state.merge({
            isSharing: false,
            isDoneSharing: true,
            shareMessage: action.message
          }).toJS();

        case Constants.CLEAR_MESSAGES:
            return state.merge({
              isSharing: false,
              isDoneSharing: false,
              shareMessage: {}
            }).toJS();

        default:
          return state.toJS();
    }
}

module.exports = update;
