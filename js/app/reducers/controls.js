var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    controls: {}
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.RECEIVE_CONTROL:
            var mergeIn = {},
                column = action.dataType+','+action.column,
                valueObjs = [];
            action.values.forEach(function(value) {
                var label = value;
                if (action.itemMap) {
                    if (action.itemMap.hasOwnProperty(value)) {
                        label = action.itemMap[value];
                    } else {
                        return
                    }
                }
                valueObjs.push({
                    "label": label,
                    "value": value
                });
            });
            mergeIn[column] = valueObjs;
            return state.mergeDeep(Immutable.fromJS({
                "controls": mergeIn
            })).toJS();
        default:
            return state.toJS();
    }
}

module.exports = update;
