var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    controls: {},
    configurationUpdates: {},
    requireRedraw: false,
    updateConfiguration: false
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.PUSH_USER_STATE:
            var outerMerge = {},
                innerMerge = {};
            outerMerge[action.target] = innerMerge;
            innerMerge[action.column] = action.values;
            if (state.hasIn([action.target, action.column])) {
                state = state.deleteIn([action.target, action.column]);
            }
            if (action.target === "configurationUpdates") {
                state = state.merge({
                    requireRedraw: true,
                    updateConfiguration: true
                });
            }
            if (action.values) {
                return state.mergeDeep(Immutable.fromJS(outerMerge)).toJS();
            } else {
                return state.toJS();
            }
        case Constants.CLEAR_ANALYSIS:
            return state.merge({
                controls: {},
                configurationUpdates: {}
            }).toJS();
        case Constants.RECEIVE_TRANSFORMED_DATA:
            return state.merge({
                requireRedraw: true
            }).toJS();
        case Constants.RECEIVE_SAVE:
            if (action.analysisSave) {
                return Immutable.fromJS(action.analysisSave.configuration.userState).merge({
                    updateConfiguration: true
                }).toJS();
            } else {
                return Immutable.fromJS(initialState).merge({
                    updateConfiguration: true
                }).toJS();
            }
        case Constants.PLOT_DRAWN:
            return state.merge({
                requireRedraw: action.redraw,
                updateConfiguration: action.updateConfiguration
            }).toJS();
        case Constants.RECEIVE_TRANSFORMED_DATA:
            return state.merge({
                requireRedraw: true
            }).toJS();
        default:
          return state.toJS();
    }
}

module.exports = update;
