var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    isFetching: false,
    isSuccessful: false,
    isAssigning: false,
    isAssignSuccess: false,
    shareMessage: {},
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_ANALYSES:
            return state.merge({
                isFetching: false,
                isSuccessful: false
            }).toJS();
        case Constants.UPLOADING_PROJECT_DATA:
            return state.merge({
                isFetching: true,
                isSuccessful: false
            }).toJS();
        case Constants.UPLOADING_PROJECT_DATA_SUCCESSFUL:
            return state.merge({
                isFetching: false,
                isSuccessful: true
            }).toJS();
        case Constants.ASSIGNING_PROJECT_DATA:
            return state.merge({
                isAssigning: true,
                isAssignSuccess: false
            }).toJS();
        case Constants.ASSIGNING_PROJECT_DATA_SUCCESSFUL:
            return state.merge({
                isAssigning: false,
                isAssignSuccess: true
            }).toJS();
        case Constants.SHARING_PROJECT:
            return state.merge({
                shareMessage: {}
            }).toJS();
        case Constants.COMPLETE_PROJECT_SHARE:
            return state.merge({
                shareMessage: action.message
            }).toJS();
        case Constants.CLEAR_MESSAGES:
            return state.merge({
                shareMessage: {},
                isAssigning: false,
                isAssignSuccess: false
            }).toJS();
        default:
          return state.toJS();
    }
}

module.exports = update;
