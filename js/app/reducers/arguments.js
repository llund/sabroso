var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    args: {}
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.RECEIVE_SAVE:
            if (action.analysisSave) {
                return action.analysisSave.configuration.args;
            } else {
                return initialState;
            }
            break;
        case Constants.CLEAR_ANALYSIS:
            return state.merge({
                args: {}
            }).toJS();
        case Constants.PUSH_USER_STATE:
            if (state.hasIn(["args", action.column])) {
                state = state.deleteIn(["args", action.column]);
            }
            switch (action.method) {
                case Constants.ControlTypes.ARGUMENT_SELECTION:
                    var newArguments = {};
                    newArguments[action.column] = [];
                    action.values.forEach(function(value) {
                        newArguments[action.column].push(value);
                    });
                    return state.mergeDeep({
                        "args": newArguments
                    }).toJS();
                default:
                    return state.toJS();
            }
        default:
          return state.toJS();
    }
}

module.exports = update;
