var Immutable = require('immutable'),
    Constants = require('../constants.js');

var initialState = {
    analysisTypes: [],
    isFetching: false
};

var update = function(state, action) {
    if (!state) {
        state = initialState;
    }
    state = Immutable.fromJS(state);
    switch (action.type) {
        case Constants.REQUEST_ALL_ANALYSIS_CONFIGURATION:
            return state.merge({
                isFetching: true,
            }).toJS();
        case Constants.RECEIVE_ALL_ANALYSIS_CONFIGURATION:
            return state.merge({
                isFetching: false,
                analysisTypes: action.analysisTypes
            }).toJS();
        default:
          return state.toJS();
    }
}

module.exports = update;
