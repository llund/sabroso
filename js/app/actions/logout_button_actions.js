cookie = require('react-cookie');
var Constants = require('../constants');

var Actions = {
  logoutUser: function logoutUser() {
    cookie.remove('token', {path: '/'});
    cookie.remove('username', {path: '/'});
    return {
      type: Constants.LOGOUT_USER,
      token: null,
      user: null,
    };
  },
};

module.exports = Actions;
