require('isomorphic-fetch');
cookie = require('react-cookie');
var Constants = require('../constants');

var Actions = {

    requestToken: function requestToken() {
        return { type: Constants.REQUEST_TOKEN };
    },

    receiveToken: function receiveToken(token, username) {
        cookie.save('token', token, {path: '/'});
        cookie.save('username', username, {path: '/'});
        return {
            type: Constants.RECEIVE_TOKEN,
            token: token,
            user: username
        };
    },

    errorReceiveToken: function errorReceiveToken(token, username) {
        return {
          type: (undefined === token) ? Constants.ERROR_RECEIVE_TOKEN : Constants.RECEIVE_TOKEN,
          token: token,
          user: username,
        };
    },

    loadCookieToken: function loadCookieToken() {
      cookieToken = cookie.load('token');
      cookieUser = cookie.load('username');
      return {
        type: Constants.RECEIVE_TOKEN,
        token: cookieToken,
        user: cookieUser,
      };
    },

    fetchToken: function fetchToken(username, password) {
        var postHeader = new Headers();
        postHeader.append("content-type", "application/json; charset=utf-8");
        return function (dispatch) {
          dispatch(Actions.requestToken());
          return fetch(
              Constants.api_urls.login_base+Constants.api_urls.get_token.url+Constants.api_urls.suffix,
              {
                  method: Constants.api_urls.get_token.method,
                  headers: postHeader,
                  dataType: 'json',
                  body: JSON.stringify({
                      "username": username,
                      "password": password
                  })
              }).then(function(json) {
                  return json.json();
              }).then(function(json) {
                  return dispatch(Actions.receiveToken(json.token, username));
              }).then(function(json) {
                  return dispatch(Actions.errorReceiveToken(json.token, username));
              })
        };
    }
};

module.exports = Actions;
