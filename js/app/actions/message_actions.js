var Constants = require('../constants');

var Actions = {
  clearMessages: function clearMessages() {
    return {
      type: Constants.CLEAR_MESSAGES
    };
  },
};

module.exports = Actions;
