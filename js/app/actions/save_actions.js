var Constants = require('../constants'),
    RestApi = require('../utils/backend_rest.js');

var Actions = {

    requestSaves: function requestSaves() {
        return { type: Constants.REQUEST_SAVES };
    },

    receiveSaves: function receiveSaves(project, analysis, saves) {
        return {
            type: Constants.RECEIVE_SAVES,
            analysis: analysis,
            project: project,
            saves: saves
        }
    },

    startShareAnalysis: function startShareAnalysis() {
      return {
        type: Constants.START_SHARE_ANALYSIS
      };
    },

    completeAnalysisShare: function completeAnalysisShare(json) {
      return {
        type: Constants.COMPLETE_ANALYSIS_SHARE,
        message: json
      };
    },
    
    fetchSaves: function fetchAnalyses(analysisId, authHeader) {
        return function (dispatch) {
            dispatch(Actions.requestSaves());
            var requestObj = {
                    "authHeader": authHeader,
                    "analysisId": analysisId
                },
                fetchAnalysis = RestApi.fetchAnalysis(requestObj),
                fetchSaves = RestApi.fetchAnalysisSaves(requestObj);
            return Promise.all([fetchAnalysis, fetchSaves]).then(function(data) {
                var analysis = data[0].analysis,
                    saves = data[1].analysisSaves.filter(function(save) {
                        return save.analysis == analysis.id;
                    });
                requestObj.analysis = analysis;
                requestObj.saves = saves;
                requestObj.projectId = analysis.project;
                return Promise.resolve(requestObj);
            }).then(function(argObj) {
                return RestApi.fetchProject(requestObj);
            }).then(function(argObj) {
                dispatch(Actions.receiveSaves(argObj.project, argObj.analysis, argObj.saves));
            });
        }
    },

    requestSave: function requestSaves() {
        return { type: Constants.REQUEST_SAVE };
    },

    receiveSave: function receiveSave(analysisSave) {
        return {
            type: Constants.RECEIVE_SAVE,
            analysisSave: analysisSave
        }
    },

    wipeActiveSave: function wipeActiveSave() {
        return {
            type: Constants.RECEIVE_SAVE,
            analysisSave: null
        }
    },

    fetchSave: function fetchSave(analysisSaveId, authHeader) {
        return function (dispatch) {
            var requestObj = {
                analysisSaveId: analysisSaveId,
                authHeader: authHeader,
            }
            dispatch(Actions.requestSave());
            return RestApi.fetchAnalysisSave(requestObj)
                .then(function(requestObj) {
                    return dispatch(Actions.receiveSave(
                        requestObj.analysisSave
                    ));
                });
        };
    },

    addSave: function addSave(save, postHeader, authHeader) {
        return function (dispatch) {
            var requestObj = {
                analysisSave: save,
                authHeader: authHeader,
                postHeader: postHeader
            }
            return RestApi.addAnalysisSave(requestObj).then(function() {
                return dispatch(Actions.fetchSaves(save.analysis, authHeader));
            });
        };
    },

    removeAnalysisSave: function removeAnalysisSave(save_id, analysis, authHeader, postHeader) {
      return function (dispatch) {
          return fetch(
              Constants.api_urls.base+Constants.api_urls.remove_analysis_save.url+save_id+'/',
              {
                  method: Constants.api_urls.remove_analysis_save.method,
                  headers: postHeader
              }).then(function() {
                  return dispatch(Actions.fetchSaves(analysis.id, authHeader));
              });
      }
    },

    shareAnalysisSave: function shareAnalysisSave(analysis_save, user, authHeader, postHeader) {
        return function (dispatch) {
            dispatch(Actions.startShareAnalysis());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.share_analysis_save.url,
                {
                    method: Constants.api_urls.share_analysis_save.method,
                    headers: postHeader,
                    body: JSON.stringify({ analysis_save: analysis_save, user: user })
                }).then(function(json) {
                    return json.json();
                }).then(function(json) {
                    return dispatch(Actions.completeAnalysisShare(json));
                });
        }
    },

};

module.exports = Actions;
