require('isomorphic-fetch');
var Immutable = require('immutable'),
    Constants = require('../constants');

var Actions = {
    requestData: function requestData() {
        return { type: Constants.REQUEST_TRANSFORMED_DATA };
    },

    receiveData: function receiveData(data, meta) {
        return {
            type: Constants.RECEIVE_TRANSFORMED_DATA,
            data: data,
            meta: meta
        };
    },

    errorReceiveData: function errorReceiveData() {
        return {
            type: Constants.ERROR_RECEIVE_TRANSFORMED_DATA,
            data: [],
            meta: [],
        };
    },

    fetchData: function fetchData(pvtCnfg, filters, args, projectId, postHeader) {
        return function (dispatch) {
            var columnData = null,
                argList = [],
                filterIdxs = {},
                argIdxs = {};

            dispatch(Actions.requestData());
            pivotConfiguration = Immutable.fromJS(pvtCnfg);
            pivotConfiguration = pivotConfiguration.set("project", projectId);
            pvtCnfg.filters.forEach(function(filtObj, i) {
                if (!filtObj.hasOwnProperty("query")) {
                    pivotConfiguration = pivotConfiguration.setIn(["filters", i, "query"], Immutable.Map());
                }
                filterIdxs[filtObj.project_data_type] = i;
            });
            pvtCnfg.transform.forEach(function(trans, i) {
                argIdxs[trans.function] = i;
            });
            Object.keys(filters.query).forEach(function(key) {
                columnData = key.split(',');
                pivotConfiguration = pivotConfiguration.setIn(["filters", filterIdxs[columnData[0]], "query", columnData[1]], filters.query[key]);
            });
            Object.keys(args.args).forEach(function(arg) {
                columnData = arg.split(',');
                argList = [];
                args.args[arg].forEach(function(val) {
                    argList.push(val.value);
                });
                if (argList.length) {
                    pivotConfiguration = pivotConfiguration.setIn(["transform", argIdxs[columnData[0]], "kwargs", columnData[1]], argList);
                }
            })
            return fetch(
                        Constants.api_urls.base+Constants.api_urls.pivot.url,
                    {
                        method: Constants.api_urls.pivot.method,
                        headers: postHeader,
                        dataType: 'json',
                        body: JSON.stringify(pivotConfiguration.toJS())
                    }).then(function(json) {
                        return json.json();
                    }).then(function(data) {
                        return dispatch(
                            Actions.receiveData(data.data, data.meta)
                        );
                    }).catch(function(error) {
                        return dispatch(Actions.errorReceiveData());
                    });
        }
    }
}

module.exports = Actions;
