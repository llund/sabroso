var Constants = require('../constants');

var Actions = {
    pushUserState: function pushUserState(state) {
        return {
            type: Constants.PUSH_USER_STATE,
            target: state.target,
            method: state.method,
            column: state.column,
            values: state.values
        };
    },

    plotRedraw: function plotRedraw(doRedraw, updateConfiguration) {
        return {
            type: Constants.PLOT_DRAWN,
            redraw: doRedraw,
            updateConfiguration: updateConfiguration
        };
    }
}

module.exports = Actions;
