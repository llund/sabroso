require('isomorphic-fetch');
var Constants = require('../constants'),
    fetchAllAnalysisConfiguration = require('./analysis_configuration_actions').fetchAllAnalysisConfiguration;

var Actions = {

    uploadingData: function uploadingData() {
        return { type: Constants.UPLOADING_PROJECT_DATA };
    },

    uploadingSuccessful: function uploadingSuccessful() {
        return { type: Constants.UPLOADING_PROJECT_DATA_SUCCESSFUL };
    },

    assigningData: function assigningData() {
      return { type: Constants.ASSIGNING_PROJECT_DATA };
    },

    assigningDataSuccessful: function assigningDataSuccessful() {
      return { type: Constants.ASSIGNING_PROJECT_DATA_SUCCESSFUL };
    },

    gettingDataList: function gettingDataList() {
      return { type: Constants.GETTING_PROJECT_DATA_LIST };
    },

    gettingDataListSuccessful: function gettingDataListSuccessful(json) {
      return {
          type: Constants.GETTING_PROJECT_DATA_LIST_SUCCESSFUL,
          data_list: json
      };
    },

    assignDataToProject: function assignDataToProject(projectId, dataType, datasetId, postHeader) {
      return function (dispatch) {
          dispatch(Actions.assigningData());
          var payload = {
            'project': projectId,
            'data_type': dataType,
            'dataset_id': datasetId
          };
          return fetch(
              Constants.api_urls.base+Constants.api_urls.assign_project_data.url,
              {
                  method: Constants.api_urls.assign_project_data.method,
                  headers: postHeader,
                  body: JSON.stringify(payload)
              }).then(function() {
                  dispatch(Actions.assigningDataSuccessful());
                  dispatch(fetchAllAnalysisConfiguration(postHeader));
              });
      }
    },

    addData: function addData(data, project, dataType, authHeader) {
        return function (dispatch) {
            dispatch(Actions.uploadingData());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.add_project_data.url+dataType+'/'+project+'/',
                {
                    method: Constants.api_urls.add_project_data.method,
                    headers: authHeader,
                    body: JSON.stringify(data)
                }).then(function() {
                    dispatch(Actions.uploadingSuccessful());
                    dispatch(fetchAllAnalysisConfiguration(authHeader));
                });
        }
    },

    listData: function listData(project, authHeader) {
        return function (dispatch) {
            dispatch(Actions.gettingDataList());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.get_project_data_list.url+project+'/'+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.get_project_data_list.method,
                    headers: authHeader,
                    dataType: 'json'
                  }).then(function(json) {
                      return json.json();
                  }).then(function(json) {
                      return dispatch(Actions.gettingDataListSuccessful(json));
                  });
        }
    }

};

module.exports = Actions;
