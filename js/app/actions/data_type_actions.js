require('isomorphic-fetch');
var Constants = require('../constants');

var Actions = {

    requestDataTypes: function requestDataTypes() {
        return { type: Constants.REQUEST_DATA_TYPES };
    },

    receiveDataTypes: function receiveDataTypes(json) {
        return {
            type: Constants.RECEIVE_DATA_TYPES,
            dataTypes: json
        }
    },

    fetchDataTypes: function fetchDataTypes(authHeader) {
        return function (dispatch) {
            dispatch(Actions.requestDataTypes());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.get_data_types.url+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.get_data_types.method,
                    headers: authHeader,
                    dataType: 'json'
                }).then(function(json) {
                    return json.json();
                }).then(function(json) {
                    return dispatch(Actions.receiveDataTypes(json.results));
                });
        }
    }
};

module.exports = Actions;
