require('isomorphic-fetch');
var Constants = require('../constants');

var Actions = {

    requestProjects: function requestProjects() {
        return { type: Constants.REQUEST_PROJECTS };
    },

    receiveProjects: function receiveProjects(json) {
        return {
            type: Constants.RECEIVE_PROJECTS,
            projects: json
        }
    },
    requestProjectsOfType: function requestProjectsOfType() {
        return { type: Constants.REQUEST_PROJECTS_OF_TYPE };
    },

    receiveProjectsOfType: function receiveProjectsOfType(json) {
        return {
            type: Constants.RECEIVE_PROJECTS_OF_TYPE,
            projects: json
        }
    },

    sharingProject: function sharingProject() {
      return {
        type: Constants.SHARING_PROJECT
      }
    },

    completeProjectShare: function completeProjectShare(json) {
        return {
            type: Constants.COMPLETE_PROJECT_SHARE,
            message: json
        }
    },

    errorReceiveProjects: function errorReceiveProjects(error) {
        return {
            type: Constants.ERROR_RECEIVE_MENU
        }
    },

    fetchProjects: function fetchProjects(authHeader) {
        return function (dispatch) {
            dispatch(Actions.requestProjects());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.get_projects.url+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.get_projects.method,
                    headers: authHeader,
                    dataType: 'json'
                }).then(function(json) {
                    return json.json();
                }).then(function(json) {
                    return dispatch(Actions.receiveProjects(json.results));
                }).catch(function(error) {
                    return dispatch(Actions.errorReceiveProjects(error));
                });
        }
    },

    fetchProjectListOfDataType: function fetchProjectListOfDataType(dataType, authHeader) {
      return function (dispatch) {
          dispatch(Actions.requestProjectsOfType());
          return fetch(
              Constants.api_urls.base+Constants.api_urls.get_projects_of_type.url+dataType+'/'+Constants.api_urls.suffix,
              {
                  method: Constants.api_urls.get_projects.method,
                  headers: authHeader,
                  dataType: 'json'
              }).then(function(json) {
                  return json.json();
              }).then(function(json) {
                  return dispatch(Actions.receiveProjectsOfType(json));
              }).catch(function(error) {
                  return dispatch(Actions.errorReceiveProjects(error));
              });
      }
    },

    addProject: function addProject(project, postHeader, authHeader) {
        return function (dispatch) {
            return fetch(
                Constants.api_urls.base+Constants.api_urls.add_project.url,
                {
                    method: Constants.api_urls.add_project.method,
                    headers: postHeader,
                    body: JSON.stringify(project)
                }).then(function() {
                    return dispatch(Actions.fetchProjects(authHeader));
                });
        }
    },

    removeProject: function removeProject(project_id, authHeader, postHeader) {
      return function (dispatch) {
          return fetch(
              Constants.api_urls.base+Constants.api_urls.remove_project.url+project_id+'/',
              {
                  method: Constants.api_urls.remove_project.method,
                  headers: postHeader
              }).then(function() {
                  return dispatch(Actions.fetchProjects(authHeader));
              });
      }
    },

    shareProject: function shareProject(project, user, authHeader, postHeader) {
        return function (dispatch) {
            dispatch(Actions.sharingProject());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.share_project.url,
                {
                    method: Constants.api_urls.share_project.method,
                    headers: postHeader,
                    body: JSON.stringify({ project: project, user: user })
                }).then(function(json) {
                    return json.json();
                }).then(function(json) {
                    return dispatch(Actions.completeProjectShare(json));
                });
        }
    },

};

module.exports = Actions;
