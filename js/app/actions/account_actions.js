require('isomorphic-fetch');

var Constants = require('../constants');

var Actions = {

    requestUsers: function requestUsers(json) {
        return {
            type: Constants.REQUEST_USERS,
            users: json
        }
    },

    receiveUsers: function receiveUsers(json) {
        return {
            type: Constants.RECEIVE_USERS,
            users: json
        }
    },

    passwordNoMatch: function passwordNoMatch() {
      return {
        type: Constants.PASSWORD_CHANGE_NOMATCH,
        message: 'Passwords do not match',
      };
    },

    resolveChangePassword: function resolveChangePassword(json) {
      if(json.success) {
        return {
          type: Constants.PASSWORD_CHANGED,
          message: json.success,
        };
      }
      else {
        var msg = (json.error) ? json.error : 'Could not change password';
        return {
          type: Constants.PASSWORD_CHANGE_ERROR,
          message: msg,
        };
      }
    },

    changePassword: function changePassword(authHeader, p1, p2) {
        return function (dispatch) {
          if( p1 !== p2 ) {
            return dispatch(Actions.passwordNoMatch());
          }
          else {
            dispatch({type: Constants.PASSWORD_CHANGE, message: 'Working...', });

            var data = {'password1': p1, 'password2': p2};

            return fetch(
                Constants.api_urls.base+Constants.api_urls.account_change_password.url+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.account_change_password.method,
                    headers: authHeader,
                    dataType: 'json',
                    body: JSON.stringify(data)
                }).then(function(response) {
                    return response.json();
                }).then(function(json) {
                    return dispatch(Actions.resolveChangePassword(json));
                });
        }
      }
    },

    fetchUsers: function fetchUsers(authHeader) {
        return function (dispatch) {
            dispatch(Actions.requestUsers());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.get_users.url+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.get_users.method,
                    headers: authHeader,
                    dataType: 'json'
                }).then(function(json) {
                    return json.json();
                }).then(function(json) {
                    return dispatch(Actions.receiveUsers(json.results));
                });
        }
    }
}

module.exports = Actions;
