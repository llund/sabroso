require('isomorphic-fetch');
var Constants = require('../constants');

var Actions = {

    requestAnalyses: function requestAnalyses() {
        return { type: Constants.REQUEST_ANALYSES };
    },

    receiveAnalyses: function receiveAnalyses(project, analyses) {
        return {
            type: Constants.RECEIVE_ANALYSES,
            analyses: analyses,
            project: project
        }
    },

    startShareAnalysis: function startShareAnalysis() {
      return {
        type: Constants.START_SHARE_ANALYSIS
      };
    },

    completeAnalysisShare: function completeAnalysisShare(json) {
      return {
        type: Constants.COMPLETE_ANALYSIS_SHARE,
        message: json
      };
    },

    errorReceiveAnalysis: function errorReceiveAnalysis(error) {
        return {
            type: Constants.ERROR_RECEIVE_MENU
        }
    },

    fetchAnalyses: function fetchAnalyses(projectId, authHeader) {
        return function (dispatch) {
            dispatch(Actions.requestAnalyses());
            var getProject = fetch(
                    Constants.api_urls.base+Constants.api_urls.get_projects.url+projectId+'/'+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.get_projects.method,
                    headers: authHeader,
                    dataType: 'json'
                }).then(function(json) {
                    return json.json();
                }).catch(function(error) {
                    return dispatch(Actions.errorReceiveAnalysis(error));
                }),
                getAnalyses = fetch(
                    Constants.api_urls.base+Constants.api_urls.get_analyses.url+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.get_analyses.method,
                    headers: authHeader,
                    dataType: 'json'
                }).then(function(json) {
                    return json.json();
                }).catch(function(error) {
                    return dispatch(Actions.errorReceiveAnalysis(error));
                });
            return Promise.all([getProject, getAnalyses]).then(function(data) {
                var project = data[0],
                    analyses = data[1].results.filter(function(analysis) {
                        return analysis.project == project.id;
                    }).map(function(analysis) {
                        analysis.configuration = JSON.parse(analysis.configuration);
                        return analysis;
                    });
                return dispatch(Actions.receiveAnalyses(project, analyses));
            });
        }
    },

    addAnalysis: function addAnalysis(analysis, postHeader, authHeader) {
        return function (dispatch) {
            return fetch(
                Constants.api_urls.base+Constants.api_urls.add_analysis.url,
                {
                    method: Constants.api_urls.add_analysis.method,
                    headers: postHeader,
                    body: JSON.stringify(analysis)
                }).then(function() {
                    return dispatch(Actions.fetchAnalyses(analysis.project, authHeader));
                });
        }
    },

    removeAnalysis: function removeAnalysis(analysis_id, project, authHeader, postHeader) {
      return function (dispatch) {
          return fetch(
              Constants.api_urls.base+Constants.api_urls.remove_analysis.url+analysis_id+'/',
              {
                  method: Constants.api_urls.remove_analysis.method,
                  headers: postHeader
              }).then(function() {
                  return dispatch(Actions.fetchAnalyses(project.id, authHeader));
              });
      }
    },

    shareAnalysis: function shareAnalysis(analysis, user, authHeader, postHeader) {
        return function (dispatch) {
            dispatch(Actions.startShareAnalysis());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.share_analysis.url,
                {
                    method: Constants.api_urls.share_analysis.method,
                    headers: postHeader,
                    body: JSON.stringify({ analysis: analysis, user: user })
                  }).then(function(json) {
                      return json.json();
                  }).then(function(json) {
                      return dispatch(Actions.completeAnalysisShare(json));
                  });
        }
    },

    clearCurrentAnalysis: function clearCurrentAnalysis() {
        return {
            type: Constants.CLEAR_ANALYSIS
        }
    },

};

module.exports = Actions;
