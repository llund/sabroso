require('isomorphic-fetch');
var Constants = require('../constants');

var Actions = {

    requestControl: function requestControl() {
        return { type: Constants.REQUEST_CONTROL };
    },

    receiveControl: function receiveControl(dataType, column, json, itemMap) {
        return {
            type: Constants.RECEIVE_CONTROL,
            dataType: dataType,
            column: column,
            values: json,
            itemMap: itemMap
        };
    },

    fetchControl: function fetchControl(dataType, project, column, authHeader, itemMap) {
        return function (dispatch) {
            dispatch(Actions.requestControl());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.get_project_data.url+dataType+'/'+project+'/'+column+'/',
                {
                    method: Constants.api_urls.get_projects.method,
                    headers: authHeader,
                    dataType: 'json'
                }).then(function(json) {
                    return json.json();
                }).then(function(json) {
                    return dispatch(Actions.receiveControl(dataType, column, json, itemMap));
                });
        }
    }
};

module.exports = Actions;
