require('isomorphic-fetch');
var Constants = require('../constants');

var Actions = {

    requestAnalysisConfiguration: function requestAnalysisConfiguration() {
        return { type: Constants.REQUEST_ANALYSIS_CONFIGURATION };
    },

    receiveAnalysisConfiguration: function receiveAnalysisConfiguration(analysisType, pluginType, plotConfig, transConfig) {
        return {
            type: Constants.RECEIVE_ANALYSIS_CONFIGURATION,
            analysisType: analysisType,
            pluginType: pluginType,
            plotConfiguration: plotConfig,
            transformationConfiguration: transConfig
        };
    },

    fetchAnalysisConfiguration: function fetchAnalysisConfiguration(analysisId, authHeader) {
        return function (dispatch) {
            dispatch(Actions.requestAnalysisConfiguration());
            return fetch(
                Constants.api_urls.base+Constants.api_urls.get_analyses.url+analysisId+'/'+Constants.api_urls.suffix,
            {
                method: Constants.api_urls.get_analyses.method,
                headers: authHeader,
                dataType: 'json'
            }).then(function(json) {
                return json.json();
            }).then(function(analysis) {
                var getAnalysisType = fetch(
                        Constants.api_urls.base+Constants.api_urls.get_analysis_types.url+analysis.analysis_type+'/'+Constants.api_urls.suffix,
                    {
                        method: Constants.api_urls.get_analysis_types.method,
                        headers: authHeader,
                        dataType: 'json'
                    }).then(function(json) {
                        return json.json();
                    }).then(function(analysisType) {
                        return dispatch(
                            Actions.receiveAnalysisConfiguration(
                                analysisType.name,
                                analysisType.plugin,
                                JSON.parse(analysisType.plot_configuration),
                                JSON.parse(analysisType.transformation_configuration)
                            )
                        );
                    });
            });
        };
    },

    requestAllAnalysisConfiguration: function requestAnalysisConfiguration() {
        return { type: Constants.REQUEST_ALL_ANALYSIS_CONFIGURATION };
    },

    receiveAllAnalysisConfiguration: function receiveAllAnalysisConfiguration(analysisTypes) {
        return {
            type: Constants.RECEIVE_ALL_ANALYSIS_CONFIGURATION,
            analysisTypes: analysisTypes
        };
    },

    fetchAllAnalysisConfiguration: function fetchAnalysisConfiguration(authHeader) {
        return function (dispatch) {
            dispatch(Actions.requestAllAnalysisConfiguration());
            var getAnalysisType = fetch(
                    Constants.api_urls.base+Constants.api_urls.get_analysis_types.url+Constants.api_urls.suffix,
                {
                    method: Constants.api_urls.get_analysis_types.method,
                    headers: authHeader,
                    dataType: 'json'
                }).then(function(json) {
                    return json.json();
                }).then(function(analysisTypes) {
                    return dispatch(
                        Actions.receiveAllAnalysisConfiguration(analysisTypes.results)
                    );
                });
        };
    }
};

module.exports = Actions;
