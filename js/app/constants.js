var Constants = {

    REQUEST_TOKEN: "REQUEST_TOKEN",
    RECEIVE_TOKEN: "RECEIVE_TOKEN",
    ERROR_RECEIVE_TOKEN: "ERROR_RECEIVE_TOKEN",
    LOGOUT_USER: "LOGOUT_USER",
    ERROR_RECEIVE_MENU: "ERROR_RECEIVE_MENU",
    REQUEST_PROJECTS: "REQUEST_PROJECTS",
    RECEIVE_PROJECTS: "RECEIVE_PROJECTS",
    REQUEST_PROJECTS_OF_TYPE: "REQUEST_PROJECTS_OF_TYPE",
    RECEIVE_PROJECTS_OF_TYPE: "RECEIVE_PROJECTS_OF_TYPE",
    SHARING_PROJECT: "SHARING_PROJECT",
    COMPLETE_PROJECT_SHARE: "COMPLETE_PROJECT_SHARE",
    REQUEST_ANALYSES: "REQUEST_ANALYSES",
    RECEIVE_ANALYSES: "RECEIVE_ANALYSES",
    REQUEST_SAVES: "REQUEST_SAVES",
    RECEIVE_SAVES: "RECEIVE_SAVES",
    REQUEST_SAVE: "REQUEST_SAVE",
    RECEIVE_SAVE: "RECEIVE_SAVE",
    START_SHARE_ANALYSIS: "START_SHARE_ANALYSIS",
    COMPLETE_ANALYSIS_SHARE: "COMPLETE_ANALYSIS_SHARE",
    REQUEST_ANALYSIS_CONFIGURATION: "REQUEST_ANALYSIS_CONFIGURATION",
    RECEIVE_ANALYSIS_CONFIGURATION: "RECEIVE_ANALYSIS_CONFIGURATION",
    REQUEST_ALL_ANALYSIS_CONFIGURATION: "REQUEST_ALL_ANALYSIS_CONFIGURATION",
    RECEIVE_ALL_ANALYSIS_CONFIGURATION: "RECEIVE_ALL_ANALYSIS_CONFIGURATION",
    REQUEST_TRANSFORMED_DATA: "REQUEST_TRANSFORMED_DATA",
    RECEIVE_TRANSFORMED_DATA: "RECEIVE_TRANSFORMED_DATA",
    ERROR_RECEIVE_TRANSFORMED_DATA: "ERROR_RECEIVE_TRANSFORMED_DATA",
    REQUEST_DATA_TYPES: "REQUEST_DATA_TYPES",
    RECEIVE_DATA_TYPES: "RECEIVE_DATA_TYPES",
    PUSH_USER_STATE: "PUSH_USER_STATE",
    REQUEST_CONTROL: "REQUEST_CONTROL",
    RECEIVE_CONTROL: "RECEIVE_CONTROL",
    FETCH_CONTROL: "FETCH_CONTROL",
    UPLOADING_PROJECT_DATA: "UPLOADING_PROJECT_DATA",
    UPLOADING_PROJECT_DATA_SUCCESSFUL: "UPLOADING_PROJECT_DATA_SUCCESSFUL",
    ASSIGNING_PROJECT_DATA: "ASSIGNING_PROJECT_DATA",
    ASSIGNING_PROJECT_DATA_SUCCESSFUL: "ASSIGNING_PROJECT_DATA_SUCCESSFUL",
    GETTING_PROJECT_DATA_LIST: "GETTING_PROJECT_DATA_LIST",
    GETTING_PROJECT_DATA_LIST_SUCCESSFUL: "GETTING_PROJECT_DATA_LIST_SUCCESSFUL",
    PLOT_DRAWN: "PLOT_DRAWN",
    PASSWORD_CHANGE: "PASSWORD_CHANGE",
    PASSWORD_CHANGED: "PASSWORD_CHANGED",
    PASSWORD_CHANGE_ERROR: "PASSWORD_CHANGE_ERROR",
    PASSWORD_CHANGE_NOMATCH: "PASSWORD_CHANGE_NOMATCH",
    REQUEST_USERS: "REQUEST_USERS",
    RECEIVE_USERS: "RECEIVE_USERS",
    CLEAR_MESSAGES: "CLEAR_MESSAGES",
    CLEAR_ANALYSIS: "CLEAR_ANALYSIS",

    ItemTypes: {
        PROJECT: 'Project',
        ANALYSIS: 'Analysis',
        PROJECT_DATA_TYPE: 'Project Data Type',
        SAVE: 'Save'
    },

    AnalysisPlugins: {
        VEGA: 'Vega',
    },

    ControlTypes: {
        SELECTION: 'Selection',
        RANGE: 'Range',
        ARGUMENT_SELECTION: 'ArgumentSelection',
        CONFIGURATION_SELECTION: 'ConfigurationSelection'
    },

    api_urls: {
        'base' : 'http://127.0.0.1:5000/v1/',
        'login_base': 'http://127.0.0.1:5000/',
        'suffix': "?format=json",
        'get_token' : {
            'url' : 'api-token-auth/',
            'method' : 'POST'
        },
        'add_project_data': {
            'url': 'project_data/',
            'method': 'POST'
        },
        'get_project_data_list': {
            'url': 'projects/data/list/',
            'method': 'GET'
        },
        'get_project_data': {
            'url': 'project_data/',
            'method': 'GET'
        },
        'assign_project_data': {
            'url': 'project_data/assign/',
            'method': 'POST'
        },
        'get_projects' : {
            'url' : 'projects/',
            'method' : 'GET'
        },
        'add_project' : {
            'url' : 'projects/',
            'method' : 'POST'
        },
        'share_project': {
            'url': 'project/share/',
            'method': 'POST'
        },
        'remove_project': {
            'url': 'projects/',
            'method': 'DELETE'
        },
        'get_projects_of_type': {
            'url': 'projects/type/',
            'method': 'GET'
        },
        'get_analyses' : {
            'url' : 'analyses/',
            'method' : 'GET'
        },
        'add_analysis' : {
            'url': 'analyses/',
            'method': 'POST'
        },
        'remove_analysis' : {
            'url': 'analyses/',
            'method': 'DELETE'
        },
        'share_analysis': {
            'url': 'analysis/share/',
            'method': 'POST'
        },
        'get_analysis_saves' : {
            'url' : 'analysis_saves/',
            'method' : 'GET'
        },
        'add_analysis_save' : {
            'url': 'analysis_saves/',
            'method': 'POST'
        },
        'remove_analysis_save' : {
            'url': 'analysis_saves/',
            'method': 'DELETE'
        },
        'share_analysis_save': {
            'url': 'analysis_save/share/',
            'method': 'POST'
        },
        'get_analysis_types' : {
            'url' : 'analysis_types/',
            'method' : 'GET'
        },
        'get_data_types' : {
            'url' : 'data_types/',
            'method' : 'GET'
        },
        'pivot': {
            'url': 'pivot/',
            'method': 'POST'
        },
        'account_change_password': {
          'url': 'account/password/',
          'method': 'POST',
        },
        'get_users': {
          'url': 'users/',
          'method': 'GET',
        },
    },

    headers: {}
};

module.exports = Constants;
