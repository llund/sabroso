var React = require("react/addons"),
    TestUtils = React.addons.TestUtils;

var utils = {
  createComponent: function createComponent(component, props, children) {
    var shallowRenderer = TestUtils.createRenderer();
    children = children === undefined ? [] : children;
    shallowRenderer.render(
        React.createElement(
            component, props, children.length > 1 ? children : children[0]
        )
    );
    return shallowRenderer.getRenderOutput();
  }
};

module.exports = utils;
