var React = require("react/addons"),
    SpecUtils = require('./spec-utils.js'),
    Item = require('./../app/components/Item.js');

describe("Item", function() {
    var renderedItem,
        itemProps = {
            name: 'name',
            description: 'description'
        };

    beforeEach(function() {
        renderedItem = SpecUtils.createComponent(Item, itemProps);
    });

});
