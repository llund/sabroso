# sabroso #

# Current version: 0.3.3 (Third prototype, full development environment) #

Sabroso is a software framework designed to make it easy to develop full-functionality, custom, web-based data visualization applications in rapid time. 

There are two core components of sabroso:

* A Python backend, implemented in Django-REST, which stores user data and performs custom, requested data transformations. The intended use of this backend is for the developer to insert fully custom data analysis pipelines for use within their application. It is designed with the use of the SciPy stack in mind, so that machine learning and complex statistical operations should be easily integrated into workflows.

* A React/Redux javascript front end, which communicates with the backend and allows for uses to push data into the system and interact with custom data visualizations defined by the developer.

System features:

* User management and authentication
* Data management through the use of projects
* Custom visualization and interactivity
* Visualization and project sharing 

We have started a wiki with information on installing, working with and implementing systems with sabroso, which can be found here:

https://bitbucket.org/accendero/sabroso/wiki/Home