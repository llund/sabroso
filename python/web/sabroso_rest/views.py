import pymongo
import json
import logging
import re
from django.contrib.auth.models import User
from django.http import JsonResponse, Http404
from django.conf import settings
from rest_framework_mongoengine import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny, DjangoModelPermissions
from bson import ObjectId, json_util

from web.sabroso_rest.serializers import (
    UserSerializer, DataTypeSerializer, AnalysisTypeSerializer, AnalysisSerializer,
    AnalysisSaveSerializer, ProjectSerializer, ProjectDataLinkSerializer)
from web.sabroso_rest.models import (
    Project, DataType, AnalysisType, Analysis, AnalysisSave, ProjectDataLink)
from application.api import transform
from web.sabroso_rest.utils.mongo import get_data_collection, get_db


def api_custom404(request):
    return JsonResponse({
      'status_code': 404,
      'error': 'Not found'
    })


def api_custom400(request):
    return JsonResponse({
      'status_code': 400,
      'error': 'Bad Request'
    })


def api_custom500(request):
    return JsonResponse({
      'status_code': 500,
      'error': 'Internal Server Error'
    })


class AllList(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method == 'GET':
            return True
        else:
            return request.user == obj

@permission_classes((DjangoModelPermissions,))
class UserViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (DjangoModelPermissions,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ProjectViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def get_queryset(self):
        user = self.request.user
        return Project.objects.filter(owner=user.id)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user.id)

    def destroy(self, request, *args, **kwargs):
        try:
            project = self.get_object()
            # remove analyses & analysis saves
            analyses = Analysis.objects.filter(project=project)
            saves = AnalysisSave.objects.filter(analysis__in=analyses).delete()
            analyses.delete()
            self.perform_destroy(project)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)

class DataTypeViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = DataType.objects.all()
    serializer_class = DataTypeSerializer


class AnalysisViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = Analysis.objects.all()
    serializer_class = AnalysisSerializer

    def destroy(self, request, *args, **kwargs):
        try:
            analysis = self.get_object()
            # remove saves
            saves = AnalysisSave.objects.filter(analysis=analysis).delete()
            self.perform_destroy(analysis)
        except Http404:
            pass
        return Response(status=status.HTTP_204_NO_CONTENT)


class AnalysisTypeViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = AnalysisType.objects.all()
    serializer_class = AnalysisTypeSerializer


class AnalysisSaveViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    queryset = AnalysisSave.objects.all()
    serializer_class = AnalysisSaveSerializer


class ProjectDataLinkViewSet(viewsets.ModelViewSet):
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    permission_classes = (IsAuthenticated,)
    serializer_class = ProjectDataLinkSerializer
    queryset = ProjectDataLink.objects.all()

    def get_queryset(self):
        user = self.request.user
        return ProjectDataLink.objects.filter(owner=user.id)

    def perform_create(self, serializer):
        project_from = Project.objects.get(id=self.request.data['dataset_id'])
        serializer.save(owner=self.request.user.id, name=project_from.name)


# will need to pass project data type in order to key project to project data collection
@api_view(['GET','PUT','POST','DELETE'])
@permission_classes((IsAuthenticated, ))
def project_data_detail(request, data_type, pk):
    logger = logging.getLogger(__name__)
    collection = get_data_collection(data_type)

    if request.method == 'GET':
        results = [data for data in collection.find({'project_id' : pk})]
        for item in results:
            item['_id'] = str(item['_id'])
        return Response(results)

    if request.method == 'POST' or request.method == 'PUT':
        project = Project.objects.get(id=pk)
        data = request.data
        for item in data:
            item[settings.PROJECT_ID_COLUMN] = pk
            item['owner'] = request.user.id
            item['title'] = project.name
        result = collection.insert_many(data)
        results = [str(item) for item in result.inserted_ids]
        return Response(results, status=status.HTTP_201_CREATED)

    if request.method == 'DELETE':
        result = collection.delete_many({"project_id" : pk})
        deleted_count = result.deleted_count
        return Response({'records-deleted':deleted_count},
                        status=status.HTTP_200_OK)


# get a list of data sets associated with this project
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def project_data_connected_list(request, pk):
    project = Project.objects.get(id=pk)

    if request.method == 'GET':
        results = project.get_connected_data()
        return Response(results)


# will need to pass project data type in order to key project to project data collection
@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def project_data_options(request, data_type, pk, column):
    logger = logging.getLogger(__name__)
    collection = get_data_collection(data_type)

    if request.method == 'GET':
        project = Project.objects.get(id=pk)
        data_type_obj = DataType.objects.get(id=data_type)
        linked_projects = ProjectDataLink.objects.filter(project=project, data_type=data_type_obj)
        linked_project_ids = [str(link.dataset_id) for link in linked_projects]
        linked_project_ids.append(pk)
        results = collection.distinct(column, {"project_id" : {"$in": linked_project_ids}})
        return Response(results)


#curl -H "Content-Type: application/json" -X POST -d '{"toDataObject": "testDataConverter", "filter":  {"query": {"keywords.name": {"$in": ["test_data"]}}, "project": "56cf636e99388e5a90a695cd", "project_data_type": "My Test Data"}, "transform": [{"function": "passThrough", "kwargs": {}}], "generateMeta": "testMetaCreator"}' http://localhost:8000/v1.0/pivot
@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def pivot_data(request):
    logger = logging.getLogger(__name__)

    if not request.data:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    json_string = json.dumps(request.data)
    transform_data = json.loads(json_string)

    try:
        pivot_data = transform(transform_data, transform_data['filters'])
    except TypeError as e:
        return Response({'error-message': str(e)},
                        status=status.HTTP_400_BAD_REQUEST)

    return Response(pivot_data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def projects_of_type(request, data_type):
    user = request.user

    if data_type is None:
        return Response(
            {'error': 'Please select a data type'},
            status=status.HTTP_400_BAD_REQUEST
        )

    collection = get_data_collection(data_type)
    results = collection.aggregate([
        {"$match":
            {"owner": user.id}
        },
        {"$group":
            {"_id": { 'project_id': "$project_id", 'title': "$title" } }
        }
    ])
    output = [r['_id'] for r in results]
    return Response(output, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def project_share(request):
    if not request.data:
        return Response(
            {'error':'Please select a user and project to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        project_id = request.data['project']
        user_id = request.data['user']
    except KeyError as ex:
        return Response(
            {'error':'Please select a user and project to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    project = Project.objects.get(id=project_id) or None
    user = User.objects.get(id=user_id) or None

    if not project or not user:
        return Response(
            {'error':'Please choose a valid project and user'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if request.user.id != project.owner:
        return Response(
            {'error':'You may only share a project you own'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if project.share_with(user):
        return Response(
            {'success':'Project successfully shared'},
            status=status.HTTP_200_OK
        )

    return Response(
        {'error':'Could not share project'},
        status=status.HTTP_400_BAD_REQUEST
    )

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def analysis_share(request):
    if not request.data:
        return Response(
            {'error':'Please select a user and analysis to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        analysis_id = request.data['analysis']
        user_id = request.data['user']
    except KeyError as ex:
        return Response(
            {'error':'Please select a user and analysis to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    analysis = Analysis.objects.get(id=analysis_id) or None
    user = User.objects.get(id=user_id) or None

    if not analysis or not user:
        return Response(
            {'error':'Please choose a valid analysis and user'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if request.user.id != analysis.project.owner:
        return Response(
            {'error':'You may only share items from a project you own'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if analysis.share_with(user):
        return Response(
            {'success':'Analysis successfully shared'},
            status=status.HTTP_200_OK
        )

    return Response(
        {'error':'Could not share analysis'},
        status=status.HTTP_400_BAD_REQUEST
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def analysis_save_share(request):
    if not request.data:
        return Response(
            {'error':'Please select a user and analysis save to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        analysis_save_id = request.data['analysis_save']
        user_id = request.data['user']
    except KeyError as ex:
        return Response(
            {'error':'Please select a user and analysis save to share'},
            status=status.HTTP_400_BAD_REQUEST
        )

    analysis_save = AnalysisSave.objects.get(id=analysis_save_id) or None
    user = User.objects.get(id=user_id) or None

    if not analysis_save or not user:
        return Response(
            {'error':'Please choose a valid analysis save and user'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if request.user.id != analysis_save.analysis.project.owner:
        return Response(
            {'error':'You may only share items from a project you own'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if analysis_save.share_with(user):
        return Response(
            {'success':'Analysis save successfully shared'},
            status=status.HTTP_200_OK
        )

    return Response(
        {'error':'Could not share analysis save'},
        status=status.HTTP_400_BAD_REQUEST
    )

@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def change_password(request):
    if not request.data:
        return Response(
            {'error':'Please fill out the form'},
            status=status.HTTP_400_BAD_REQUEST
        )

    p1 = request.data['password1'] or None
    p2 = request.data['password2'] or None

    if not p1 or not p2:
        return Response(
            {'error':'Please enter new password and confirm'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if p1 != p2:
        return Response(
            {'error':'Passwords do not match'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if len(p1) < 1:
        return Response(
            {'error':'Password must be at least 1 character'},
            status=status.HTTP_400_BAD_REQUEST
        )

    if not re.search('[a-zA-Z0-9]', p1):
        return Response(
            {'error':'Password must conatin at least a number or letter'},
            status=status.HTTP_400_BAD_REQUEST
        )

    user = request.user
    user.set_password(p1)
    user.save()

    return Response({'success':'Password changed successfully'}, status=status.HTTP_200_OK)
