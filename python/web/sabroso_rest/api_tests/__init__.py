from djsabroso.api.api_tests.project_tests import *
from djsabroso.api.api_tests.project_data_tests import *
from djsabroso.api.api_tests.analysis_type_tests import *
from djsabroso.api.api_tests.analysis_tests import *
from djsabroso.api.api_tests.user_tests import *
from djsabroso.api.api_tests.api_tests import *
from mongoengine import register_connection


register_connection(alias='default', name='sabroso_django_testing')
