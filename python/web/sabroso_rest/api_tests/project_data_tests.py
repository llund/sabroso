from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from djsabroso.api.serializers import ProjectSerializer
from djsabroso.api.models import Project
import logging
import pymongo
import json
import time

tu = 'test_user'
tp = 'test_password'
te = 'test_user@accendero.com'

data_fixtures = [
    {
        "type": "proteinSequence",
        "data": "MAEEEEPPPLLKDDD",
        "hash": "c24b3e4ba6e94a3ebc79e4506b0cabac",
        "keywords": {
            "id": 1,
            "subgroup": 0
        },
        "annotations": {
            "similarity": {
                "60c8a6f0d2a940fdbe3d6860659d045e": 1E50,
                "f3c4bcd9fa2ad699e6c739834a0b9639": 1E4,
                "f82d75f5ea1efd02f033a40bee4bf631": 1E3
            }
        }
    },
    {
        "type": "proteinSequence",
        "data": "MTTYPLPMKLHNNNG",
        "hash": "60c8a6f0d2a940fdbe3d6860659d045e",
        "keywords": {
            "id": 2,
            "subgroup": 0
        },
        "annotations": {
            "similarity": {
                "c24b3e4ba6e94a3ebc79e4506b0cabac": 1E50,
                "f3c4bcd9fa2ad699e6c739834a0b9639": 1E5,
                "f82d75f5ea1efd02f033a40bee4bf631": 1E4
            }
        }
    },
    {
        "type": "proteinSequence",
        "data": "MYGYFWAAAGGGLPS",
        "hash": "f3c4bcd9fa2ad699e6c739834a0b9639",
        "keywords": {
            "id": 3,
            "subgroup": 1
        },
        "annotations": {
            "similarity": {
                "c24b3e4ba6e94a3ebc79e4506b0cabac": 1E4,
                "60c8a6f0d2a940fdbe3d6860659d045e": 1E5,
                "f82d75f5ea1efd02f033a40bee4bf631": 1E7
            }
        }
    },
    {
        "type": "proteinSequence",
        "data": "MKKRRKRRKWEDEDES",
        "hash": "f82d75f5ea1efd02f033a40bee4bf631",
        "keywords": {
            "id": 4,
            "subgroup": 2
        },
        "annotations": {
            "similarity": {
                "c24b3e4ba6e94a3ebc79e4506b0cabac": 1E3,
                "60c8a6f0d2a940fdbe3d6860659d045e": 1E4,
                "f3c4bcd9fa2ad699e6c739834a0b9639": 1E7
            }
        }
    }
]

"""
TODO: make test data end up in a test database, currently going into whatever
default mongo collection we have for project_data
"""
class AllProjectDataTest(APITestCase):
    def setUp(self):
        self.logger = logging.getLogger("unittests")
        self.user = User.objects.create_user(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.data = {'name': 'TestProjectDataCreateProject'}
        self.data_fixtures = data_fixtures

        response = self.client.post('/v1/projects/', self.data)
        if response.status_code != 201:
            self.fail()

        self.project = Project.objects.get(id=json.loads(response.content)['id'])
        self.url = '/v1/projects/' + str(self.project.id) + '/data/'

        try:
            mongo_connection = \
                pymongo.MongoClient('mongodb://localhost')
        except pymongo.errors.ConnectionFailure as e:
            return None

        self.db = mongo_connection['sabroso_django_testing']

    def test_can_create_project_data(self):
        response = self.client.post(self.url, self.data_fixtures)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_can_append_project_data(self):
        response = self.client.post(self.url, self.data_fixtures)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_can_delete_project_data(self):
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
