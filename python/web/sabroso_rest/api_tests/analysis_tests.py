from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from djsabroso.api.models import *
from djsabroso.api.serializers import AnalysisSerializer
import logging
import json


tu = 'test_user'
tp = 'test_password'
te = 'test_user@accendero.com'


class CreateAnalysisTest(APITestCase):
    def setUp(self):
        self.logger = logging.getLogger("unittests")
        self.user = User.objects.create_user(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.project = Project.objects.create(name='Test Project')
        self.analysis_type = AnalysisType.objects.create(
            name='Test Analysis Type',
            data_types='A, B, C',
            configuration="{'abc': 123, 'def': true}"
        )
        self.data = {
            'name': 'Test Analysis',
            'owner': tu,
            'description': 'Analysis for unittests',
            'configuration': "{'abc': 'doStuff', 'def': 1}",
            'project': str(self.project.id),
            'analysis_type': str(self.analysis_type.id)
        }

    def test_can_create_analysis(self):
        response = self.client.post('/v1/analyses/', self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def tearDown(self):
        Project.drop_collection()
        AnalysisType.drop_collection()
        Analysis.drop_collection()


class ReadAnalysisTest(APITestCase):
    def setUp(self):
        self.logger = logging.getLogger("unittests")
        self.user = User.objects.create_user(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.project = Project.objects.create(name='Test Project')
        self.analysis_type = AnalysisType.objects.create(
            name='Test Analysis Type',
            data_types='A, B, C',
            configuration="{'abc': 123, 'def': true}"
        )
        self.analysis = Analysis.objects.create(
            name='Test Analysis',
            owner=tu,
            description='Analysis for unittests',
            configuration="{'abc': 'doStuff', 'def': 1}",
            project=self.project,
            analysis_type=self.analysis_type
        )

    def test_can_list_analyses(self):
        response = self.client.get('/v1/analyses/')
        self.logger.error(response.content)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_get_analysis_detail(self):
        response = self.client.get('/v1/analyses/' + str(self.analysis.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def tearDown(self):
        Project.drop_collection()
        AnalysisType.drop_collection()
        Analysis.drop_collection()

class UpdateAnalysisTest(APITestCase):
    def setUp(self):
        self.logger = logging.getLogger("unittests")
        self.user = User.objects.create_user(tu, te, tp)
        self.client.login(username=tu, password=tp)
        self.project = Project.objects.create(name='Test Project')
        self.analysis_type = AnalysisType.objects.create(
            name='Test Analysis Type',
            data_types='A, B, C',
            configuration="{'abc': 123, 'def': true}"
        )
        self.analysis = Analysis.objects.create(
            name='Test Analysis',
            owner=tu,
            description='Analysis for unittests',
            configuration="{'abc': 'doStuff', 'def': 1}",
            project=self.project,
            analysis_type=self.analysis_type
        )
        self.data = AnalysisSerializer(self.analysis).data

    def test_can_update_analysis(self):
        analysis = self.analysis
        self.logger.error(analysis.name)
        self.data['name'] = 'Test Analysis - Updated Name'
        response = self.client.put('/v1/analyses/' + str(self.analysis.id), self.data)
        self.analysis = Analysis.objects.get(id=self.analysis.id)
        self.logger.error(self.analysis.name)
        self.logger.error(analysis.name)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # def test_can_delete_analysis(self):
    #     response = self.client.delete('/v1/analyses/' + str(self.analysis.id))
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
