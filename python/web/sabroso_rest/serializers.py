from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework_mongoengine.serializers import DynamicDocumentSerializer
from rest_framework.validators import UniqueTogetherValidator
from web.sabroso_rest.models import (
    DataType, AnalysisType, Analysis, AnalysisSave, Project, ProjectDataLink)

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'password', 'is_superuser', 'is_staff')
        read_only_fields = ('id',)
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username'],
            is_superuser=validated_data['is_superuser'],
            is_staff=validated_data['is_staff'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class DataTypeSerializer(DynamicDocumentSerializer):
    class Meta:
        model = DataType


class ProjectDataLinkSerializer(DynamicDocumentSerializer):
    class Meta:
        model = ProjectDataLink
        fields = ('id', 'name', 'project', 'data_type', 'owner', 'dataset_id')


class AnalysisTypeSerializer(DynamicDocumentSerializer):
    data_types = DataTypeSerializer(many=True, read_only=True)
    active_projects = serializers.ListField(read_only=True)

    class Meta:
        model = AnalysisType


class AnalysisSerializer(DynamicDocumentSerializer):
    class Meta:
        model = Analysis
        fields = (
            'id', 'name', 'description', 'configuration',
            'created_on', 'project', 'analysis_type'
        )


class AnalysisSaveSerializer(DynamicDocumentSerializer):
    class Meta:
        model = AnalysisSave


class ProjectSerializer(DynamicDocumentSerializer):
    analyses = AnalysisSerializer(many=True, read_only=True)
    # owner = serializers.SerializerMethodField('get_owner')
    #
    # def get_owner(self, obj):
    #     self.owner = self.context['request'].user.id
    #     return self.context['request'].user.id

    class Meta:
        model = Project
        fields = ('id', 'owner', 'name', 'description', 'created_on', 'analyses')
