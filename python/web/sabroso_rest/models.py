from __future__ import unicode_literals

from mongoengine import (
    StringField, DateTimeField, IntField,
    ListField, ReferenceField, DynamicDocument)
import datetime
import logging
from django.conf import settings
from web.sabroso_rest.utils.mongo import *

class Project(DynamicDocument):
    name = StringField(max_length=128, required=True)
    description = StringField(max_length=255)
    owner = IntField()
    shared_by = IntField()
    created_on = DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.name

    def get_data(self, data_type, query, return_cursor=False):
        logger = logging.getLogger(__name__)

        try:
            linked_data = ProjectDataLink.objects.filter(project=self, data_type=data_type)
        except ProjectDataLink.DoesNotExist as ex:
            linked_data = None
            pass

        linked_project_ids = [l.dataset_id for l in linked_data]
        linked_project_ids.append(str(self.id))
        query['project_id'] = {"$in": linked_project_ids}
        data_type_id = str(data_type.id)
        add_collection = get_data_collection(data_type_id)

        if return_cursor:
            return add_collection.find(query)
        else:
            return [data for data in add_collection.find(query)]

    def has_data_of_type(self, data_type):
        collection = get_data_collection(data_type)
        result = collection.count({'project_id': str(self.id)})
        return result > 0

    def get_connected_data(self):
        data_set = []
        for link in ProjectDataLink.objects.filter(project=self):
            data_set.append({'key': str(link.id), 'value': "[LINK] %s" %(link)})

        for data_type in DataType.objects.all():
            if self.has_data_of_type(str(data_type.id)):
                data_set.append({'key': str(data_type.id), 'value': "[SELF] %s" %(data_type.name)})
        return data_set

    def share_with(self, user):
        # copy project
        project = Project(
            name=self.name,
            description=self.description,
            owner=user.id,
            shared_by=self.owner
        )

        if not project.save():
            return False

        # copy data links
        for link in ProjectDataLink.objects.filter(project=self):
            add_link = ProjectDataLink(
                project=project,
                name=link.name,
                data_type=link.data_type,
                dataset_id=link.dataset_id
            )
            add_link.save()

        # create links to non-linked data
        for data_type in DataType.objects.all():
            if self.has_data_of_type(str(data_type.id)):
                add_link = ProjectDataLink(
                    project=project,
                    name=project.name,
                    data_type=data_type,
                    dataset_id=str(self.id)
                )
                add_link.save()
        return project


class DataType(DynamicDocument):
    name = StringField(max_length=64, required=True)
    created_on = DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return self.name

class ProjectDataLink(DynamicDocument):
    project = ReferenceField(Project)
    owner = IntField()
    data_type = ReferenceField(DataType)
    dataset_id = StringField(max_length=255)
    name = StringField(max_length=255)

    def __unicode__(self):
        return str(self.name) + " / " + str(self.data_type)


class AnalysisType(DynamicDocument):
    name = StringField(max_length=128, required=True)
    data_types = ListField(ReferenceField(DataType))
    plot_configuration = StringField()
    transformation_configuration = StringField()
    plugin = StringField(max_length=255)

    def __unicode__(self):
        return self.name

    def active_projects(self):
        projects = None
        for data_type in self.data_types:
            collection = get_data_collection(str(data_type.id))
            dt_projects = set(collection.distinct(settings.PROJECT_ID_COLUMN))
            linked_project_ids = set([str(link.project.id) for link in ProjectDataLink.objects.filter(data_type=data_type)])
            if projects is None:
                projects = dt_projects | linked_project_ids
            else:
                projects = projects | dt_projects | linked_project_ids
        return list(projects)


class Analysis(DynamicDocument):
    project = ReferenceField(Project)
    analysis_type = ReferenceField(AnalysisType)
    name = StringField(max_length=128, required=True)
    description = StringField()
    configuration = StringField()
    created_on = DateTimeField(default=datetime.datetime.now)

    def share_with(self, user):

        share_project = self.project.share_with(user)

        if not share_project:
            return False

        analysis = Analysis(
            project=share_project,
            analysis_type=self.analysis_type,
            name=self.name,
            description=self.description,
            configuration=self.configuration
        )

        if not analysis.save():
            return False

        return analysis

    def __unicode__(self):
        return self.name


class AnalysisSave(DynamicDocument):
    analysis = ReferenceField(Analysis)
    configuration = StringField()
    created_on = DateTimeField(default=datetime.datetime.now)
    name = StringField(max_length=128, required=True)
    description = StringField()

    def share_with(self, user):

        share_analysis = self.analysis.share_with(user)

        if not share_analysis:
            return False

        analysis_save = AnalysisSave(
            analysis=share_analysis,
            configuration=self.configuration,
            name=self.name,
            description=self.description
        )

        if not analysis_save.save():
            return False

        return analysis_save

    def __unicode__(self):
        return self.name
