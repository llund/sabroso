import pandas

def cursorToDataFrame(cursor, max_rows=1000):
    df, rows = None, []
    while 1:
        try:
            item = cursor.next()
        except StopIteration:
            if len(rows):
                new_df = pandas.DataFrame(rows)
                if df is None:
                    df = new_df
                else:
                    df = pandas.concat([df, new_df])
            break
        else:
            if len(rows) > max_rows:
                new_df = pandas.DataFrame(rows)
                if df is None:
                    df = new_df
                else:
                    df = pandas.concat([df, new_df])
                rows = []
            rows.append(item)
    return df
