"""
api.py

common API module for application, expected to contain
certain functions:
    transform(pivot_config, session)
"""

__author__ = "Alan Barber"

import logging

from .functions import registry
from web.sabroso_rest.models import Project, DataType
from web.sabroso_rest.utils.mongo import get_data_collection

def transform(pivot_config, collection):
    logger = logging.getLogger(__name__)

    # Shorthand
    filter_pms = pivot_config['filters']
    pivot = pivot_config['transform']
    pivot_meta_function = pivot_config['generateMeta']
    finalize_data = pivot_config['finalizeData']
    project = pivot_config['project']

    try:
        project = Project.objects.get(id=project)
    except:
        raise TypeError('Project does not exist')

    # Load project data
    all_data = {}
    for one_filt in filter_pms:
        data_type = DataType.objects.get(id=one_filt["project_data_type"])
        data_type_name = data_type.name
        # all_data[data_type_name] = project.get_data(get_data_collection(one_filt["project_data_type"]), one_filt['query'], return_cursor=True)
        all_data[data_type_name] = project.get_data(data_type, one_filt['query'], return_cursor=True)

    # Convert to our data object using specified function
    toDataObject = registry.get(pivot_config['toDataObject'])
    finalObject = toDataObject(all_data)

    # Apply pivot functions
    for f in pivot:
        function_name = f['function']
        arguments = f['kwargs']
        if len(arguments):
            finalObject = registry.get(function_name)(finalObject, **arguments)
        else:
            finalObject = registry.get(function_name)(finalObject)

    # Generate our meta data for our finalObject
    objectMetaData = registry.get(pivot_meta_function)(finalObject)

    # Generate final form
    finalObject = registry.get(finalize_data)(finalObject)

    # Put our data and meta-data together for shipping
    pivotObject = {'meta': objectMetaData, 'data': finalObject}

    return pivotObject
