"""
setup_test_application.py

This script loads test data into the REST api database for acceptance testing.
"""

__author__ = "Alan Barber"

#Built in
import argparse
import datetime
import json
import hashlib

#3rd party
import pymongo

project_name = "Test project"
data = [12, 23, 47, 6, 52, 19]
analysis_type_name = "Test analysis"
configuration = {
    "testPlot": {
        "visualization": {
            "width": 400,
            "height": 400,
            "scales": [{
                "name": "r",
                "type": "sqrt",
                "domain": {"data": "data", "field": "data"},
                "range": [20, 100]
                }],
            "marks": [{
                "type": "arc",
                "from": {"data": "data"},
                "properties": {
                    "enter": {
                        "x": {"field": {"group": "width"}, "mult": 0.5},
                        "y": {"field": {"group": "height"}, "mult": 0.5},
                        "startAngle": {"field": "layout_start"},
                        "endAngle": {"field": "layout_end"},
                        "innerRadius": {"value": 20},
                        "outerRadius": {"scale": "r", "field": "data"},
                        "stroke": {"value": "#fff"}
                    },
                    "update": {
                        "fill": {"value": "#ccc"}
                    },
                    "hover": {
                        "fill": {"value": "pink"}
                    }
                }
            },
            {
                "type": "text",
                "from": {"data": "data"},
                "properties": {
                "enter": {
                    "x": {"field": {"group": "width"}, "mult": 0.5},
                    "y": {"field": {"group": "height"}, "mult": 0.5},
                    "radius": {"scale": "r", "field": "data", "offset": 8},
                    "theta": {"field": "layout_mid"},
                    "fill": {"value": "#000"},
                    "align": {"value": "center"},
                    "baseline": {"value": "middle"},
                    "text": {"field": "plot1"}
                    }
                }
            }]
        },
        "request": {
            "filter": {
                "project": project_name,
                "query": {
                    "keywords.name": {"$in": ["test_data"]}
                }
            },
            "toDataObject": "testDataConverter",
            "generateMeta": "testMetaCreator",
            "transform": [{
                "function": "passThrough",
                "kwargs": {}
            }]
        }
    }
}

def add_project(db, project_name):
    collection = db['project']
    collection.update_one(
        {'name': project_name},
        {'$set': {
            'name': project_name,
            'created_on': str(datetime.datetime.now())
        }}, upsert=True)
    project = collection.find({
        'name': project_name
    }).next()
    return str(project['_id'])

def add_data(db, project_id, data):
    to_update = {
        "type": "test_data",
        "keywords": {
            "name": "test_data"
        },
        "annotations": {}
    }
    m = hashlib.md5()
    m.update(repr(data).encode('utf-8'))
    to_update['hash'] = m.hexdigest()
    to_update['data'] = data
    collection = db["project_data_"+project_id]
    collection.update_one(
        {"keywords": to_update['keywords']},
        {'$set': to_update},
        upsert=True)

def add_analysis_type(db, name, conf):
    to_update = {
        "name": name,
        "data_types": ["test_data"],
        "configuration": json.dumps(conf)
    }
    collection = db['analysistype']
    collection.update_one(
        {"name": name},
        {'$set': to_update},
        upsert=True)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Add test data to REST database.')
    parser.add_argument(
        '--database_name', default="sabroso",
        help='database name')
    args = parser.parse_args()
    client = pymongo.MongoClient()
    db = client[args.database_name]

    project_id = add_project(db, project_name)
    add_data(db, project_id, data)
    add_analysis_type(db, analysis_type_name, configuration)
