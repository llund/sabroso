"""
functions.py

application functions to be imported into registry
"""

from .utils.registry import Registry
import numpy
import pandas
from .utils.pandas import cursorToDataFrame

registry = Registry('TestFunctions')

#Registered functions
def testDataConverter(data):
    ret = []
    for oneSet in data:
        for datum in data[oneSet]:
            del datum['_id']
            ret.append(datum)
    return pandas.DataFrame(ret)


def testMetaCreator(data):
    return []


def passThrough(data, *args, **kwargs):
    try:
        col = kwargs["column"]
        data[col] = data[col] + 1
    except KeyError:
        pass

    return data


def dataFrameToDict(data, **kwargs):
    ret = []
    data = data.dropna(axis=0)
    for i, row in data.iterrows():
        ret.append(row.to_dict())
    return ret

registry.add('testDataConverter', testDataConverter)
registry.add('testMetaCreator', testMetaCreator)
registry.add('passThrough', passThrough)
registry.add('dataFrameToDict', dataFrameToDict)
