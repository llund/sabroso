"""
functions.py

application functions to be imported into registry
"""

from .utils.registry import Registry
import numpy
import pandas
from .utils.pandas import cursorToDataFrame

registry = Registry('TestFunctions')

#Registered functions
def testDataConverter(data):
    ret = []
    for oneSet in data:
        for datum in data[oneSet]:
            del datum['_id']
            ret.append(datum)
    return pandas.DataFrame(ret)

def listDataConverter(data):
    ret = []
    for cursor in data:
        for row in data[cursor]:
            del row['_id']
            ret.append(row)
    return ret

def getListByGroups(data, *args, **kwargs):
    groups = kwargs['groups']
    keyname = kwargs['keyname']
    ret = []
    for row in data:
        if row[keyname] in groups:
            ret.append(row)
    return ret

def testMetaCreator(data):
    return []

def passDataThrough(data):
    return data

def passThrough(data, *args, **kwargs):
    try:
        col = kwargs["column"]
        data[col] = data[col] + 1
    except KeyError:
        pass
    return data

def dataFrameToDict(data, **kwargs):
    ret = []
    data = data.dropna(axis=0)
    for i, row in data.iterrows():
        ret.append(row.to_dict())
    return ret

registry.add('testDataConverter', testDataConverter)
registry.add('listDataConverter', listDataConverter)
registry.add('getListByGroups', getListByGroups)
registry.add('testMetaCreator', testMetaCreator)
registry.add('passThrough', passThrough)
registry.add('dataFrameToDict', dataFrameToDict)
registry.add('passDataThrough', passDataThrough)
