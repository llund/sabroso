"""
functions.py

application functions to be imported into registry
"""

from .utils.registry import Registry
import numpy
import pandas
from .utils.pandas import cursorToDataFrame
from lifelines.statistics import logrank_test
import lifelines

registry = Registry('TestFunctions')

#Registered functions
def testDataConverter(data):
    ret = []
    for oneSet in data:
        for datum in data[oneSet]:
            del datum['_id']
            ret.append(datum)
    return pandas.DataFrame(ret)


def survivalTransformer(data, *args, **kwargs):
    groups = kwargs['groups']
    keyname = kwargs['keyname']

    T = data["duration"]
    C = data["observed"]

    data_set = {}
    for group in groups:
        ds = (data[keyname] == group)
        data_set[group] = { 'ds': ds, 'T': T[ds], 'C': C[ds] }
    return data_set

def survivalMetaCreator(data):
    ret = []
    for group, ds in data.items():
        test_result = logrank_test(ds['T'], ds['C'], alpha=0.95)
        ret.append({
            'Name': group,
            'Value': "{0:10.3f}".format(test_result.p_value)
        })
    return ret

def testMetaCreator(data):
    return []

def passThrough(data, *args, **kwargs):
    try:
        col = kwargs["column"]
        data[col] = data[col] + 1
    except KeyError:
        pass
    return data

def survivalFinalizer(data, **kwargs):
    kmf = lifelines.KaplanMeierFitter()
    ret = []
    censored = {}
    series = {}
    for group, ds in data.items():
        fitter = kmf.fit(ds['T'], event_observed=ds['C'])
        censored[group] = fitter.event_table.to_dict()['censored']
        series[group] = fitter.survival_function_.to_dict()

    for group_name, km in series.items():
        items = sorted(km['KM_estimate'].items())
        l = len(items) - 1

        for i in range(0, l):
            item = items[i]
            timeline = item[0]
            value = item[1]

            if i < l-1:
                datum = {
                    "TimeStart": timeline,
                    "TimeEnd": items[i+1][0],
                    "PercentSurvivalStart": value,
                    "PercentSurvivalEnd": items[i+1][1],
                    "Group": group_name
                }
            else:
                datum = {
                    "TimeStart": timeline,
                    "TimeEnd": items[i+1][0],
                    "PercentSurvivalStart": value,
                    "Group": group_name
                }

            if i > 0 and group_name in censored:
                censored_item = censored[group_name][timeline]
                censored_datum = {
                    "Group": group_name,
                    "Time": timeline,
                    "PercentSurvival": items[i-1][1]
                }
                ret.append(censored_datum)

            ret.append(datum)
    return ret

def dataFrameToDict(data, **kwargs):
    ret = []
    data = data.dropna(axis=0)
    for i, row in data.iterrows():
        ret.append(row.to_dict())
    return ret

registry.add('testDataConverter', testDataConverter)
registry.add('testMetaCreator', testMetaCreator)
registry.add('passThrough', passThrough)
registry.add('dataFrameToDict', dataFrameToDict)
registry.add('survivalTransformer', survivalTransformer)
registry.add('survivalMetaCreator', survivalMetaCreator)
registry.add('survivalFinalizer', survivalFinalizer)
