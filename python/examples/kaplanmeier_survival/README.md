Kaplan-Meier Survival Visualization Example
--

#### Config

**_Note:_** *The following steps will overwrite your existing analysis configuration*

The first step in setting up our visualization is loading the configuration file
(`config.yml`) found within the example directory `python/examples/kaplanmeier_survival/`.
From the `python/web/` directory, run the following command to load our config file:
```
python manage.py addadmindata ../examples/kaplanmeier_survival/config.yml
```

#### Code
**_Note:_** *The following steps will replace your registered functions file*

Once we have our configuration loaded, we can copy our `functions.py` from
`python/examples/kaplanmeier_survival/` to `python/application/`. To ensure that
these functions will work, install the requirements in the provided `requirements.txt`
file:
```
pip install -r python/examples/kaplanmeier_survival/requirements.txt
```

#### Data
Now that we have our basic functionality in place, we need to export the data
for our example from the lifelines dataset. Enter into an interactive python
session. Run through the following code to create a csv of our lifelines
dataset in the current working directory
```python
import lifelines
ds = lifelines.datasets.load_dd()
ds.to_csv('lifelines_dd.csv')
```

#### Run Sabroso
Get Sabroso up and running, and then create a new project. Add a new project, and
navigate into this project. Under the `Upload data` menu, select the `survival`
data type. Click `Choose File` and browse to our `lifelines_dd.csv` file. Click
`Upload`. Once your data is uploaded, add an Analysis, making sure to select the
`survival_analysis` type. Click into your new analysis and you should now have
a working survival visualization to work with.
