data_types:
    - test_type
    - test_type_2
    - survival

analysis_types:
    -
        name: survival_analysis
        data_types:
            - survival
        plot_configuration:
            "controls":
                "filters":
                    -
                        "column": "start_year"
                        "method": "Range"
                        "data_type": "survival"
                        "label": "Year range"
                "arguments":
                    -
                        "function_name": "survivalTransformer"
                        "argument": "groups"
                        "label": "Regimes"
                        "multiple_select": True
                        "options":
                            - 'Mixed Dem'
                            - 'Monarchy'
                            - 'Military Dict'
                            - 'Presidential Dem'
                            - 'Parliamentary Dem'
                            - 'Civilian Dict'
                        "option_labels":
                            - 'Mixed Democracy'
                            - 'Monarchy'
                            - 'Military Dictatorship'
                            - 'Presidential Democracy'
                            - 'Parliamentary Democracy'
                            - 'Civilian Dictatorship'
                "configuration":
                    -
                        "label": "Color by"
                        "options":
                            -
                                - "category20"
                            -
                                - "category20b"
                            -
                                - "category20c"
                        "option_labels":
                            - "Color Scheme 1"
                            - "Color Scheme 2"
                            - "Color Scheme 3"
                        "paths":
                            -
                                - scales
                                - 2
                                - range
            "plot":
                "width": 600
                "height": 600
                "data":
                    -
                        "name": "table"
                    -
                        "name": "meta"
                        "transform":
                            -
                                "type": "formula"
                                "field": "Title"
                                "expr": "datum.Name+': '+datum.Value"
                    -
                        "name": "curves"
                        "source": "table"
                        "transform":
                            -
                                "type": "filter"
                                "test": "datum.PercentSurvivalStart"
                    -
                        "name": "censored"
                        "source": "table"
                        "transform":
                            -
                                "type": "filter"
                                "test": "datum.PercentSurvival"
                "scales":
                    -
                        "name": "xscale"
                        "type": "linear"
                        "domainMin": 0
                        "domainMax": {"data": "curves","field": "TimeEnd"}
                        "range": "width"
                        "nice": true
                    -
                        "name": "yscale"
                        "type": "linear"
                        "domain": [0,1]
                        "range": [600,0]
                        "nice": true
                    -
                        "name": "colorscale"
                        "type": "ordinal"
                        "domain": {"data": "curves","field": "Group"}
                        "range": "category20"
                    -
                        "name": "metascale"
                        "type": "ordinal"
                        "domain": {"data": "meta", "field": "Title"}
                        "range": "category20"
                "axes":
                    - {"type": "x", "scale": "xscale", "orient": "bottom", "title": "Time (years)"}
                    - {"type": "y", "scale": "yscale", "orient": "left", "title": "Percent survival"}
                "legends":
                    -
                        "fill": "colorscale"
                        "title": "Group"
                        "offset": -200
                    -
                        "fill": "metascale"
                        "title": "P-values"
                        "offset": -200
                        "properties": {"symbols": {"fillOpacity": {"value": 0}, "stroke": {"value": "transparent"}}}
                "marks":
                    -
                        "type": "rule"
                        "from": {"data": "curves"}
                        "properties":
                            "enter":
                                "x": {"field": "TimeStart","scale": "xscale"}
                                "x2": {"field": "TimeEnd","scale": "xscale"}
                                "y":
                                    "field": "PercentSurvivalStart"
                                    "scale": "yscale"
                                "stroke": {"field": "Group","scale": "colorscale"}
                                "strokeWidth": {"value": 2}
                    -
                        "type": "rule"
                        "from": {"data": "curves"}
                        "properties":
                            "enter":
                                "x": {"field": "TimeEnd","scale": "xscale"}
                                "y":
                                    "field": "PercentSurvivalStart"
                                    "scale": "yscale"
                                "y2":
                                    "field": "PercentSurvivalEnd"
                                    "scale": "yscale"
                                "stroke": {"field": "Group", "scale": "colorscale"}
                                "strokeWidth": {"value": 2}
                    -
                        "type": "rule"
                        "from": {"data": "censored"}
                        "properties":
                            "enter":
                                "x":
                                    "field": "Time"
                                    "scale": "xscale"
                                    "offset": 0
                                "y":
                                    "field": "PercentSurvival"
                                    "scale": "yscale"
                                    "offset": 5
                                "y2":
                                    "field": "PercentSurvival"
                                    "scale": "yscale"
                                    "offset": -5
                                "stroke": {"field": "Group","scale": "colorscale"}
                                "strokeWidth": {"value": 2}
                    -
                        "type": "rule"
                        "from": {"data": "censored"}
                        "properties":
                            "enter":
                                "x":
                                    "field": "Time"
                                    "scale": "xscale"
                                    "offset": -5
                                "x2":
                                    "field": "Time"
                                    "scale": "xscale"
                                    "offset": 5
                                "y":
                                    "field": "PercentSurvival"
                                    "scale": "yscale"
                                "stroke": {"field": "Group","scale": "colorscale"}
                                "strokeWidth": {"value": 2}
        transformation_configuration:
            "toDataObject": "testDataConverter"
            "filters": [{"query": {}, "project_data_type": "survival"}]
            "transform": [{"function": "survivalTransformer", "kwargs": {"groups": ['Mixed Dem'], "keyname": "regime"}}]
            "generateMeta": "survivalMetaCreator"
            "finalizeData": "survivalFinalizer"
        plugin: Vega
    -
        name: test_analysis_type
        data_types:
            - test_type
        plot_configuration:
            "controls":
                # "filters": [{"column": "z", "method": "Range"}, {"column": "a", "method": "Selection"}]
                "arguments":
                    -
                        "function_name": "passThrough"
                        "argument": "column"
                        "label": "Add One to Column"
                        "options":
                            - "x"
                            - "y"
                            - "z"
                        "option_labels":
                            - "Col X"
                            - "Col Y"
                            - "Col Z"
                "filters": [{"column": "a", "method": "Selection", "data_type": "test_type", "label": "Column A"}]
                "configuration":
                    -
                        "label": "Color by"
                        "options":
                            -
                                - ["orange", "purple"]
                            -
                                - ["blue", "green"]
                        "option_labels":
                            - "Orange, Purple"
                            - "Blue, Green"
                        "paths":
                            -
                                - scales
                                - 2
                                - range
            "plot":
                "width": 600
                "height": 600
                "data": [{"name": "table"}, {"name": "meta"}]
                "scales":
                    - {"name": "xscale", "type": "linear", "domain": {"data": "table", "field": "x"}, "range": "width", "nice": true}
                    - {"name": "yscale", "type": "linear", "domain": {"data": "table", "field": "y"}, "range": "height", "nice": true}
                    - {"name": "colorscale", "type": "linear", "domain": {"data": "table", "field": "z"}, "range": ["blue", "green"]}
                "axes":
                    - {"type": "x", "scale": "xscale", "orient": "bottom"}
                    - {"type": "y", "scale": "yscale", "orient": "left"}
                "marks":
                    -
                        "type": "symbol"
                        "from": {"data": "table"}
                        "properties":
                            "enter":
                                "x": {"field": "x", "scale": "xscale"}
                                "y": {"field": "y", "scale": "yscale"}
                                "size": {"value": 100}
                                "opacity": {"value":0.6}
                            "update":
                                "fill": {"field": "z", "scale": "colorscale"}
                            "hover": {"fill": {"value":"red"}}
                    -
                        "type": "text"
                        "properties":
                            "enter": {"fill": {"value": "black"}}
                            "update":
                                "x": {"scale": "xscale", "signal": "hover.x", "offset": -10}
                                "y": {"scale": "yscale", "signal": "hover.y", "offset":20}
                                "text": {"template": "{{hover.x}}, {{hover.y}}"}
                "signals":
                    -
                        "name": "hover"
                        "init": {}
                        "streams": [{"type": "symbol:mouseover", "expr": "datum"}, {"type": "symbol:mouseout", "expr": "{}"}]
        transformation_configuration:
            "toDataObject": "testDataConverter"
            "filters": [{"query": {}, "project_data_type": "test_type"}]
            "transform": [{"function": "passThrough", "kwargs": {}}]
            "generateMeta": "testMetaCreator"
            "finalizeData": "dataFrameToDict"
        plugin: Vega
